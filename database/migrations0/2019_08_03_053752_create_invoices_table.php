\<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('courier_id')->nullable();
            $table->unsignedBigInteger('discount_id')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->unsignedBigInteger('payable_price')->default(0);
            $table->unsignedBigInteger('orders_price')->default(0);
            $table->unsignedBigInteger('total_price')->default(0);
            $table->string('address',512);
            $table->unsignedBigInteger('user_address_id');
            $table->enum('paid',['None','Cash','Card']);
            $table->string('description')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('user_address_id')->references('id')->on('user_addresses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
