<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->string('id', '12')->primary()->unique();
            $table->string('title');
            $table->unsignedTinyInteger('percent')->default(0);
            $table->unsignedBigInteger('count');
            $table->integer('per_user');
            $table->boolean('status')->default(0);
            $table->unsignedBigInteger('min')->default(0);
            $table->unsignedBigInteger('max');
            $table->dateTime('expire_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discounts');
    }
}
