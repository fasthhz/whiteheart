<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index('invoices_user_id_foreign');
            $table->unsignedBigInteger('courier_id')->nullable();
            $table->unsignedBigInteger('discount_id')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->unsignedBigInteger('payable_price')->default(0);
            $table->unsignedBigInteger('orders_price')->default(0);
            $table->unsignedBigInteger('total_price')->default(0);
            $table->string('address', 512);
            $table->enum('paid', ['None', 'Cash', 'Card']);
            $table->string('description')->nullable();
            $table->timestamp('get_ready_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
