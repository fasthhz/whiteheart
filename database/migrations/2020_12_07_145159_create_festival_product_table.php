<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFestivalProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festival_product', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id')->index('festival_product_product_id_foreign');
            $table->unsignedBigInteger('festival_id')->index('festival_product_festival_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('festival_product');
    }
}
