    @include('layouts.courier.header')
    <!--sidebar start-->
    @yield('sidebar')
    <!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            @yield('main-content')
        </section>
    </section>
    <!--main content end-->
    @include('layouts.courier.footer')
