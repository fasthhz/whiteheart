<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{env('APP_NAME')}} - @yield('pageTitle')</title>
    <link rel="stylesheet" href="/html/css/responcive.css">
    <link rel="stylesheet" href="/html/css/all.min.css">
    <link rel="stylesheet" href="/html/css/bootstrap.min.css">
    <link rel="stylesheet" href="/html/css/swiper-bundle.min.css">
    <link rel="stylesheet" href="/html/stayle.css">
    <link rel="stylesheet" href="/html/css/responcive.css">
	<script src="/html/js/swiper-bundle.min.js"></script>

    <script src="/html/js/jquery.js"></script>
</head>

<body>
<div class="top-header">
    <div class="container">

        <p style="text-align: left">
            <a href="{{route('about_us')}}">درباره ما</a>
            |
            <a href="{{route('contact_us')}}">تماس با ما</a>

            <!--// @if(auth()->user())
            //     <a href="{{route('dashboard')}}">مشاهده پروفایل</a>
            // @endif-->

        </p>
        <i class="fas fa-bars"></i>
    </div>
    <div class="mobb">
        <div class="m1">
            <ul>
                <li>
                    <a href="{{route('about.branches')}}"> <i class="fas fa-star"></i>معرفی شعب</a>

                </li>
                <li>
                    <a href="{{route('branch.request')}}"><i class="fas fa-user"></i>اعطای نمایندگی</a>

                </li>
                <li>
                    <a href="{{route('support')}}"> <img src="/html/img/live-chat.png" alt=""> پشتیبانی</a>

                </li>
                <li>
                    <a href="{{route('cooperation.request')}}"> <img src="/html/img/hospital.png" alt=""> درخواست همکاری</a>

                </li>
            </ul>
        </div>
    </div>
</div>


<div class="nav-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12" id="m1">
                <a href="{{(request()->get('directGet')&&request()->get('branch_id')?route('about.branches').'?directGet='.request()->directGet.'&branch_id='.request()->get('branch_id'):route('about.branches'))}}"> <i class="fas fa-star"></i>معرفی شعب</a>
                <a href="{{(request()->get('directGet')&&request()->get('branch_id')?route('branch.request').'?directGet='.request()->directGet.'&branch_id='.request()->get('branch_id'):route('branch.request'))}}"><i class="fas fa-user"></i>اعطای نمایندگی</a>
            </div>
            <div class="col-md-4 col-sm-12" id="m2">
                <div class="logo">
                    <img style="cursor: pointer" onclick="function f() {
                      window.location.href = '{{(request()->get('directGet')&&request()->get('branch_id')?route('home').'?directGet='.request()->directGet.'&branch_id='.request()->get('branch_id'):route('home'))}}'
                    }f()" src="/html/img/logo.png" alt="">
                    <!--<p>سفارش خود را انتخاب کنید</p>-->
                </div>
            </div>
            <div class="col-md-4 col-sm-12" id="m3">
                <a href="{{(request()->get('directGet')&&request()->get('branch_id')?route('support').'?directGet='.request()->directGet.'&branch_id='.request()->get('branch_id'):route('support'))}}"> <img src="/html/img/live-chat.png" alt=""> پشتیبانی</a>
                <a href="{{(request()->get('directGet')&&request()->get('branch_id')?route('cooperation.request').'?directGet='.request()->directGet.'&branch_id='.request()->get('branch_id'):route('cooperation.request'))}}"> <img src="/html/img/hospital.png" alt=""> درخواست همکاری</a>
            </div>
        </div>
    </div>
</div>

<div class="nav-mob">

    <div class="logo">
        <img src="/html/img/logo.png" alt="">
        <p>سفارش خود را انتخاب کنید</p>
    </div>
</div>

@yield('content')

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md4 col-sm-12">
                <div class="tehran">
                    <h3>آدرس شعب تهران</h3>
                    <h4>بالاتر از میدان کاج،خیابان عقبری دوم، نبش بلوار بهزاد
                        <br>
                        شمالی،مقابل بوستان شقایق
                        تلفن:02122066664
                        <br>
                        خیابان اندرزگو
                        <br>
                        تلفن:02122066664
                    </h4>
                </div>

            </div>
            <div class="col-md4 col-sm-12">
                <div class="esfahan">
                    <h3>آدرس شعب اصفهان</h3>
                    <h4>
                        خیابان سعادت آباد، حنب بانک تجارت
                        <br>
                        تلفن:03136610737
                        <br>
                        خیابان مصلی،هایپرمی
                        <br>
                        تلفن:03136610737
                    </h4>
                </div>
            </div>
            <div class="col-md4 col-sm-12">
                <div class="footer-logo">
                    <img src="/html/img/logo-footer.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Initialize Swiper -->

    <script>
        $(".fas.fa-bars").click(function () {
            $(".mobb").toggle('slow');
            event.stopPropagation();
        });


    </script>
<script>

    var swiper = new Swiper('.swiper-container2', {
        slidesPerView: 5,
        spaceBetween: 10,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            // when window width is >= 480px
            480: {
                slidesPerView: 5,
                spaceBetween: 20
            },
            // when window width is >= 640px
            640: {
                slidesPerView: 5,
                spaceBetween: 20
            }
        }
    });
</script>
@stack('styles')
<script src="/html/js/jquery-3.3.1.slim.min.js"></script>
<script src="/html/js/popper.min.js"></script>
<script src="/html/js/bootstrap.min.js"></script>
@stack('scripts')

</body>

</html>
