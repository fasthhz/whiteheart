</section>

<!-- js placed at the end of the document so the pages load faster -->
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="/js/jquery.scrollTo.min.js"></script>
<script src="/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="/js/jquery.sparkline.js" type="text/javascript"></script>
<script src="/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script type="text/javascript" src="/assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="/assets/data-tables/DT_bootstrap.js"></script>
<script src="/js/owl.carousel.js"></script>
<script src="/js/jquery.customSelect.min.js"></script>

<!--common script for all pages-->
<script src="/js/common-scripts.js"></script>
<!--script for this page-->

@yield('custom-style')
@include('flash_message')
@include('notification')

<script>
    //owl carousel
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true

        });
    });
    //custom select box
    $(function () {
        $('select.styled').customSelect();
    });
    <!--Sweet alert-->
</script>
</body>
</html>
