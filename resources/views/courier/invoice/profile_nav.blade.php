<section class="panel">
    <ul class="nav nav-pills nav-stacked">
        <li class="{{isActive(['courier.dashboard.invoices.show'])}}"><a href="{{route('courier.dashboard.invoices.show',$invoice->id)}}"> <i
                    class="icon-user"></i> فاکتور</a></li>
        <li class="{{isActive(['courier.dashboard.invoices.edit'])}}"><a href="{{route('courier.dashboard.invoices.edit',$invoice->id)}}"> <i
                    class="icon-user"></i>
                ویرایش فاکتور</a></li>
    </ul>
</section>
