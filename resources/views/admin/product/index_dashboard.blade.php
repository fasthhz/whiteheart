@extends('admin.frame')
@section('custom-style')
    <script src="/js/jquery-ui-1.9.2.custom.min.js"></script>

    <!--custom switch-->
    <script src="/js/bootstrap-switch.js"></script>
    <script src="/js/jquery.tagsinput.js"></script>

    <!--custom checkbox & radio-->
    <script type="text/javascript" src="/js/ga.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-daterangepicker/date.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="/assets/ckeditor/ckeditor.js"></script>

    <script src="/js/form-component.js"></script>
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فیلتر ها
                </header>
                <div class="panel-body">
                    <form class="form-horizontal tasi-form" method="GET" action="">
                        <!--date picker start-->
                        <div class="form-group">

                            <div class="col-lg-2">
                                <label class="control-label ">ایدی </label>
                                <input name="id" type="text" size="16" class="form-control">
                                <label class="control-label ">عنوان </label>
                                <input name="title" type="text" size="16" class="form-control">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">بازه قیمت</label>
                                از<input name="startPrice" type="number" size="16" class="form-control">
                                الی<input name="endPrice" type="number" size="16" class="form-control">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">بازه تعداد</label>
                                از<input name="startQuantity" type="number" size="16" class="form-control">
                                الی<input name="endQuantity" type="number" size="16" class="form-control">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label">بازه تخفیف</label>
                                از<input name="startDiscount" type="number" size="16" class="form-control">
                                الی<input name="endDiscount" type="number" size="16" class="form-control">
                            </div>
                            <div class="col-lg-2">
                                <label class="control-label ">تعداد نمایش </label>
                                <input name="qty" type="number" value="10" size="16" class="form-control">
                                <label class="control-label ">گروه </label>
                                <select name="subcategoryId" class="form-control bound-s">
                                    <option value="">---</option>

                                @foreach($subcategories as $subcategory)
                                        <option value="{{$subcategory->id}}">{{$subcategory->title}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-info" type="submit">جستجو</button>
                            </div>
                        </div>
                        <!--date picker end-->
                    </form>


                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    محصول ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th>عنوان</th>
                        <th>گروه</th>
                        <th>تعداد</th>
                        <th>قیمت</th>
{{--                        <th>تخفیف</th>--}}
                        <th>وضعیت</th>
                        <th>شعب</th>
                        <th>تاریخ ثبت</th>
                        <th>تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($products)
                        @foreach($products as $product)
                            <tr class="gradeX odd">
                                <td>
                                    <a href="{{route('admin.dashboard.products.show',[$product->id])}}">{{$product->id}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin.dashboard.products.show',[$product->id])}}">{{$product->title}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin.dashboard.categories.show',[$product->id])}}">{{$product->subcategory->title}}</a>
                                </td>

                                <td>{{$product->quantity}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->status?'فعال':'غیر فعال'}}</td>
                                <td>
                                    @foreach($product->branches as $branch)
                                        {{$branch->name}} -
                                    @endforeach
                                </td>
                                <td>{{$product->created_at_tehran}}</td>
                                <td>{{$product->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
