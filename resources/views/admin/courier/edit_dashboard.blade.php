@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            @include('admin.courier.profile_nav')
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات پیک
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>کد :</span> {{$courier->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نام :</span> {{$courier->name}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>کد ملی:</span> {{$courier->national_number}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>موبایل :</span> {{$courier->mobile}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$courier->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$courier->updated_at_tehran}}</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    فرم ویرایش
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" enctype="multipart/form-data" method="POST"
                              action="{{route('admin.dashboard.couriers.update',$courier->id)}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')

                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">نام</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="name" minlength="2" type="text"
                                           required="" value="{{$courier->name}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">گد ملی</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="national_number"  type="number"
                                           required="" value="{{$courier->national_number}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">موبایل</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="mobile"  type="text"
                                           required="" value="{{$courier->mobile}}">
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">تصویر شخصی</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="avatar"  type="file"
                                           value="{{$courier->avatar}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.couriers.destroy',$courier->id)}}"
                              novalidate="novalidate">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
        </aside>

    </div>
@endsection
