@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">

        <aside class="profile-info col-lg-12">
            <section class="panel">
                <div class="bio-graph-heading">
                    جزییات نظر
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$comment->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>کاربر :</span><a
                                    href="{{route('admin.dashboard.users.show',['id',$comment->user->id])}}"> {{$comment->user->name}} {{$comment->user->family}} </a></p>
                        </div>
                        <div class="bio-row">
                            <p><span>خریدار محصول :</span>
                                {{$comment->is_buyer?'بله':'خیر'}}
                            </p>
                        </div>
                        <div class="bio-row">
                            <p><span>محصول :</span> <a
                                    href="{{route('admin.dashboard.pistachios.show',['id',$comment->pistachio->id])}}"> {{$comment->pistachio->title}}</a>
                            </p>
                        </div>
                        <div class="bio-row">
                            <p><span>امتیاز :</span> {{$comment->rate?:'----'}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>وضعیت :</span> {{$comment->approved?'منتشر شده':'در انتظار تایید'}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>متن :</span> {{$comment->text?:'----'}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$comment->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$comment->updated_at_tehran}}</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    ویرایش نظر
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.comments.update',['id' => $comment->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">وضعیت</label>
                                <div class="col-lg-10">
                                    <select name="status" id="minbeds" class="form-control bound-s">
                                        <option value="0" @if(!$comment->approved)selected @endif><span
                                                class="label label-danger">در انتظار</span></option>
                                        <option value="1" @if(!!$comment->approved)selected @endif><span
                                                class="label label-success">منتشر شده</span></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.comments.destroy',['id' => $comment->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>

        </aside>
    </div>
@endsection
