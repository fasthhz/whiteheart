@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img src="@if($subcategory->image) {{route('images.show',[$subcategory->image->id])}} @else /img/profile-avatar.jpg @endif" alt="">
                    </a>
                    <h1>{{$subcategory->title}}</h1>
                    <p>{{$subcategory->description}}</p>
                </div>

                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.subcategories.show',[ $subcategory->id])}}"> <i
                                class="icon-user"></i> دسته</a></li>
                    <li><a href="{{route('admin.dashboard.subcategories.edit',[ $subcategory->id])}}"> <i
                                class="icon-user"></i>
                            ویرایش دسته</a></li>
                </ul>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات دسته
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$subcategory->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>عنوان :</span> {{$subcategory->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>سردسته :</span> {{$subcategory->category->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>الویت :</span> {{$subcategory->priority}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>توضیحات :</span> {{$subcategory->description}}</p>
                        </div>

                        <div class="bio-row">
                            <p><span>تاریخ عضویت :</span> {{$subcategory->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$subcategory->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
        </aside>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    محصولات ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th>عنوان</th>
                        <th>تعداد</th>
                        <th>قیمت</th>
                        <th>تخفیف</th>
                        <th>وضعیت</th>
                        <th>الویت</th>
                        <th>توضیحات</th>
                        <th>تاریخ ثبت</th>
                        <th>تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($subcategory->products)
                        @foreach($subcategory->products as $products)
                            <tr class="gradeX odd">
                                <td>
                                    <a href="{{route('admin.dashboard.products.show',[$products->id])}}">{{$products->id}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin.dashboard.products.show',[$products->id])}}">{{$products->title}}</a>
                                </td>
                                <td>{{$products->quantity}}</td>
                                <td>{{$products->price}}</td>
                                <td>{{$products->discount}}</td>
                                <td>{{$products->status?'فعال':'غیر فعال'}}</td>
                                <td>{{$products->priority}}</td>
                                <td>{{$products->description}}</td>
                                <td>{{$products->created_at_tehran}}</td>
                                <td>{{$products->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>

    </div>
@endsection
