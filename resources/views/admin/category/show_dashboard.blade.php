@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <div class="user-heading round">
                    <a href="#">
                        <img
                            src="@if($category->image) {{route('images.show',[$category->image->id])}} @else /img/profile-avatar.jpg @endif"
                            alt="">
                    </a>
                    <h1>{{$category->title}}</h1>
                    <p>{{$category->description}}</p>
                </div>

                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.categories.show',[$category->id])}}">
                            <i
                                class="icon-user"></i> دسته</a></li>
                    <li><a href="{{route('admin.dashboard.categories.edit',[ $category->id])}}"> <i
                                class="icon-user"></i>
                            ویرایش دسته</a></li>
                </ul>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات دسته
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$category->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>عنوان :</span> {{$category->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>توضیحات :</span> {{$category->description}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>زیر دسته ها :</span>
                                @foreach($category->subcategories as $subcategory)
                                    <a href="{{route('admin.dashboard.subcategories.show',$subcategory->id)}}">{{$subcategory->title}} </a>
                                    ,
                                @endforeach
                            </p></div>
                        <div class="bio-row">
                            <p><span>الویت :</span> {{$category->priority}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ عضویت :</span> {{$category->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$category->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
        </aside>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    محصولات ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th>عنوان</th>
                        <th>گروه</th>
                        <th>تعداد</th>
                        <th>قیمت</th>
                        <th>تخفیف</th>
                        <th>وضعیت</th>
                        <th>الویت</th>
                        <th>توضیحات</th>
                        <th>تاریخ ثبت</th>
                        <th>تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($category->products)
                        @foreach($category->products()->orderByDesc('priority')->get() as $product)
                            <tr class="gradeX odd">
                                <td>
                                    <a href="{{route('admin.dashboard.products.show',[$product->id])}}">{{$product->id}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin.dashboard.products.show',[$product->id])}}">{{$product->title}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin.dashboard.subcategories.show',[$product->id])}}">{{$product->subcategory->title}}</a>
                                </td>
                                <td>{{$product->quantity}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->discount}}</td>
                                <td>{{$product->status?'فعال':'غیر فعال'}}</td>
                                <td>{{$product->priority}}</td>
                                <td>{{$product->description}}</td>
                                <td>{{$product->created_at_tehran}}</td>
                                <td>{{$product->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>

    </div>
@endsection
