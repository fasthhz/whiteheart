@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            @include('admin.invoice.profile_nav')
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات فاکتور
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$invoice->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>کاربر :</span> {{$invoice->user->name}} {{$invoice->user->family}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>وضعیت پرداخت :</span> {{$invoice->paid}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>قیمت کل :</span> {{$invoice->total_price}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>قیمت کالاها :</span> {{$invoice->orders_price}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>قیمت پرداختی :</span> {{$invoice->payable_price}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>کدتخفیف :</span> {{isset($invoice->discount)?$invoice->discount->title:'نداشته'}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تخفیف کل :</span> {{$invoice->total_price-$invoice->payable_price}}</p>
                        </div>

                        <div class="bio-row">
                            <p><span>وضعیت :</span>{{$invoice->statusLabel}}</p>
                        </div>
                        @foreach($invoice->products as $products)
                            <div class="bio-row">
                                <p><span>{{$products->title}} :</span> {{$products->pivot->count}} عدد</p>
                            </div>
                        @endforeach
                        <div class="bio-row">
                            <p><span>توضیحات :</span> {{$invoice->description}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$invoice->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$invoice->updated_at_tehran}}</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    فرم ویرایش محصول
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.invoices.update',$invoice->id)}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">وضعیت</label>
                                <div class="col-lg-10">
                                    <select name="status" id="minbeds" class="form-control bound-s">
                                        <option value="0"  @if($invoice->status==0) selected @endif ><span
                                                    class="label label-danger">ثبت شده</span></option>
                                        <option value="1" @if($invoice->status==1) selected @endif ><span
                                                class="label label-success">درحال اماده سازی</span></option>
                                        <option value="2" @if($invoice->status==2) selected @endif ><span
                                                class="label label-success" >ارسال شده</span></option>
                                        <option value="3" @if($invoice->status==3) selected @endif ><span
                                                class="label label-success">رد شده</span></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">زمان اماده سازی</label>
                                <div class="col-lg-3">
                                    <input type="number" value="{{$invoice->get_ready_at}}" name="get_ready_at">
                                </div>
{{--
                                <label for="cname" class="control-label ">({{Morilog\Jalali\Jalalian::fromCarbon(\Carbon\Carbon::createFromFormat('Y-d-m H:i:s',$invoice->get_ready_at)->timezone('Asia/Tehran'))}}زمان تخمین زده شده )</label>
--}}
                                <label for="cname" class="control-label ">(دقت کنید که زمان وارد شده باید به دقیقه باشد)</label>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.invoices.destroy',[ $invoice->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>

        </aside>
    </div>
@endsection
