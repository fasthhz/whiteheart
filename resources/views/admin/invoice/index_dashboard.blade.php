@extends('admin.frame')
@section('custom-style')
    <script src="/js/jquery-ui-1.9.2.custom.min.js"></script>

    <!--custom switch-->
    <script src="/js/bootstrap-switch.js"></script>
    <script src="/js/jquery.tagsinput.js"></script>

    <!--custom checkbox & radio-->
    <script type="text/javascript" src="/js/ga.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-daterangepicker/date.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="/assets/ckeditor/ckeditor.js"></script>

    <script src="/js/form-component.js"></script>
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')

    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فیلتر ها
                </header>
                <div class="panel-body">
                    <form class="form-horizontal tasi-form" method="GET" action="">
                        <!--date picker start-->
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-3">
                                    <label class="control-label ">ایدی </label>
                                    <input name="id" type="text" size="16" class="form-control">
                                    <label class="control-label ">وضعیت </label>
                                    <input name="status" type="number" size="16" class="form-control">
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label">بازه قیمت</label>
                                    از<input name="startPrice" type="number" size="16" class="form-control">
                                    الی<input name="endPrice" type="number" size="16" class="form-control">
                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label ">کاربر </label>
                                    <input name="user" type="text" size="16" class="form-control">

                                </div>
                                <div class="col-lg-3">
                                    <label class="control-label ">تعداد نمایش </label>
                                    <input name="qty" type="number" value="10" size="16" class="form-control">
                                    <button class="btn btn-info" type="submit">جستجو</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فاکتور ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th>کاربر</th>
                        <th>قیمت پرداختی</th>
                        <th>قیمت کل</th>
                        <th>وضعیت</th>
                        <th>توضیحات</th>
                        <th>تاریخ ثبت</th>
                        <th>تاریخ بروزرسانی</th>
                    </tr>
                    </thead>
                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($invoices)
                        @foreach($invoices as $invoice)
                            <tr class="gradeX odd">
                                <td>
                                    <a href="{{route('admin.dashboard.invoices.show',[$invoice->id])}}">{{$invoice->id}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin.dashboard.users.show',[$invoice->user->id])}}">{{$invoice->user->name}} {{$invoice->user->family}}</a>
                                </td>
                                <td>{{$invoice->payable_price}}</td>
                                <td>{{$invoice->total_price}}</td>
                                <td>{{$invoice->statusLabel}}</td>
                                <td>{{$invoice->description}}</td>
                                <td>{{$invoice->created_at_tehran}}</td>
                                <td>{{$invoice->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
