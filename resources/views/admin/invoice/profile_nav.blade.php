<section class="panel">
    <ul class="nav nav-pills nav-stacked">
        <li class="{{isActive(['admin.dashboard.invoices.show'])}}"><a href="{{route('admin.dashboard.invoices.show',$invoice->id)}}"> <i
                    class="icon-user"></i> فاکتور</a></li>
        <li class="{{isActive(['admin.dashboard.invoices.edit'])}}"><a href="{{route('admin.dashboard.invoices.edit',$invoice->id)}}"> <i
                    class="icon-user"></i>
                ویرایش فاکتور</a></li>
    </ul>
</section>
