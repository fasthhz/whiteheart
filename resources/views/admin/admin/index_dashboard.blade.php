@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    ادمین ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th class="hidden-phone">نام و نام خانوادگی</th>
                        <th class="hidden-phone">نام کاربری</th>
                        <th class="hidden-phone">تاریخ عضویت</th>
                        <th class="hidden-phone">تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($users)
                        @foreach($users as $user)
                            <tr class="gradeX odd">
                                <td class=" "><a href="{{route('admin.dashboard.admins.show',[ $user->id])}}">{{$user->id}}</a></td>
                                <td class=" "><a href="{{route('admin.dashboard.admins.show',[ $user->id])}}">{{$user->name}} {{$user->family}}</a></td>
                                <td class="center hidden-phone "><a href="{{route('admin.dashboard.admins.show',[ $user->id])}}">{{$user->username}}</a></td>
                                <td class="center hidden-phone ">{{$user->created_at_tehran}}</td>
                                <td class="center hidden-phone ">{{$user->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
