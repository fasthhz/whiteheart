<section class="panel">
    <div class="user-heading round">
        <a href="#">
            <img src="/img/profile-avatar.jpg" alt="">
        </a>
        <h1>{{$user->name}} {{$user->family}}</h1>
        <p>{{$user->mobile}}</p>
    </div>

    <ul class="nav nav-pills nav-stacked">
        <li class="{{isActive(['admin.dashboard.admins.show'])}}"><a href="{{route('admin.dashboard.admins.show',[$user->id])}}"> <i
                    class="icon-user"></i> پروفایل</a></li>
        <li class="{{isActive(['admin.dashboard.admins.edit'])}}"><a href="{{route('admin.dashboard.admins.edit',[ $user->id])}}"> <i
                    class="icon-user"></i>
                ویرایش پروفایل</a></li>
    </ul>
</section>
