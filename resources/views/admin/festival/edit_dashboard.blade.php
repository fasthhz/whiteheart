@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                @if($festival->image)
                    <div class="container">
                        <h2>عکس ها</h2>
                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="{{route('images.show',['id'=>$festival->image->id])}}"
                                         alt="{{$festival->image->original_name}}" style="width:100%;">
                                </div>

                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                @endif


                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.festivals.show',['id' => $festival->id])}}"> <i
                                class="icon-user"></i> دسته</a></li>
                    <li><a href="{{route('admin.dashboard.festivals.edit',['id' => $festival->id])}}"> <i
                                class="icon-user"></i>
                            ویرایش دسته</a></li>
                </ul>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    عکس
                </div>
                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.festivals.update',['id'=>$festival->id])}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">عکس</label>
                                <div class="col-lg-8">
                                    <input type="file" name="image">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" name="addImage" value="true" class="btn btn-success">افزودن
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.festivals.update',['id'=>$festival->id])}}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">عکس ها</label>
                                <div class="col-lg-8">
                                    <select name="image" multiple="" class="form-control">
                                        @if($festival->image)
                                            <option
                                                value="{{$festival->image->id}}">{{$festival->image->original_name}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" name="deleteImage" value="true" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات دسته
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$festival->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>عنوان :</span> {{$festival->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>توضیحات :</span> {{$festival->description}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>محصولات :</span>{{$festival->pistachios->count()}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ عضویت :</span> {{$festival->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$festival->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    فرم ویرایش
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.festivals.update',['id' => $festival->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">عنوان</label>
                                <div class="col-lg-10">
                                    <input class=" form-control" id="cname" name="title" minlength="2" type="text"
                                           required="" value="{{$festival->title}}">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="cname" class="control-label col-lg-2">توضیحات</label>
                                <div class="col-lg-10">
                                    <textarea class="form-control" name="description" cols="60"
                                              rows="5">{{$festival->description}}</textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.festivals.destroy',['id' => $festival->id])}}"
                              novalidate="novalidate">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">حذف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
            <section class="panel">
                <header class="panel-heading">
                    مدیرت محصولات
                </header>
                <div class="panel-body">
                    <div class="panel-body bio-graph-info">
                        <form class="form-horizontal" role="form"
                              action="{{route('admin.dashboard.festivals.update',['id'=>$festival->id])}}"
                              method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <select name="item" multiple="" class="form-control">
                                            @isset($pistachios)
                                                @foreach($pistachios as $pistachio)
                                                    <option value="{{$pistachio->id}}">{{$pistachio->title}}</option>
                                                @endforeach
                                            @endisset
                                        </select>

                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <div class="col-lg-offset-2 col-lg-10">
                                                <button class="btn btn-success" name="addItem" value="true"
                                                        type="submit">
                                                    اضافه
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-2 col-lg-10">
                                                <button class="btn btn-danger" name="deleteItem" value="true"
                                                        type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">
                                                    حذف
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <select name="item" multiple="" class="form-control" >
                                            @isset($festival->pistachios)
                                                @foreach($festival->pistachios as $pistachio)
                                                    <option value="{{$pistachio->id}}">{{$pistachio->title}}</option>
                                                @endforeach
                                            @endisset
                                        </select>
                                    </div>


                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </aside>

    </div>
@endsection
