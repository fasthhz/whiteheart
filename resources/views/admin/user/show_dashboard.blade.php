@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        @include('admin.user.profile_nav')
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات کاربر
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>ایدی :</span> {{$user->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>نام و نام خانوادگی :</span> {{$user->name}} {{$user->family}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>شماره موبایل :</span> {{$user->mobile}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>ایمیل :</span> {{$user->email}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ عضویت :</span> {{$user->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$user->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
        </aside>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    خرید ها
                    </header>

                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th class="hidden-phone">مبلغ کل</th>
                        <th class="hidden-phone">مبلغ قابل پرداخت</th>
                        <th class="hidden-phone">وضعیت</th>
                        <th class="hidden-phone">ادرس</th>
                        <th class="hidden-phone">تاریخ ثبت</th>
                        <th class="hidden-phone">تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($user->invoices)
                        @foreach($user->invoices as $invoice)
                            <tr class="gradeX odd">
                                <td class=" "><a href="{{route('admin.dashboard.invoices.show',[$invoice->id])}}">{{$invoice->id}}</a></td>
                                <td class="hidden-phone ">{{$invoice->total_price}}</td>
                                <td class="hidden-phone ">{{$invoice->payable_price}}</td>
                                <td class="center hidden-phone ">{{$invoice->status}}</td>
                                <td class="center hidden-phone ">{{$invoice->address}}</td>
                                <td class="center hidden-phone ">{{$invoice->created_at_tehran}}</td>
                                <td class="center hidden-phone ">{{$invoice->updated_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    تراکنش ها
                </header>

                <table class="table table-striped border-top" id="sample_2">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th>ایدی فاکتور</th>
                        <th class="hidden-phone">درگاه</th>
                        <th class="hidden-phone">قیمت</th>
                        <th class="hidden-phone">ref id</th>
                        <th class="hidden-phone">وضعیت</th>
                        <th class="hidden-phone">ip</th>
                        <th class="hidden-phone">تاریخ شروع</th>
                        <th class="hidden-phone">تاریخ پایان</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @foreach($user->getTransactions() as $transaction)
                        <tr class="gradeX odd">
                            <td class=" ">{{$transaction->id}}</td>
                            <td class=" "><a href="{{route('admin.dashboard.invoices.show',[$transaction->transactionable_id])}}">{{$transaction->transactionable_id}}</a></td>
                            <td class=" ">{{$transaction->port}}</td>
                            <td class="center hidden-phone ">{{$transaction->price}}</td>
                            <td class="center hidden-phone ">{{$transaction->ref_id}}</td>
                            <td class="center hidden-phone ">{{$transaction->status}}</td>
                            <td class="center hidden-phone ">{{$transaction->ip}}</td>
                            <td class="center hidden-phone ">{{$transaction->created_at_tehran}}</td>
                            <td class="center hidden-phone ">{{$transaction->updated_at_tehran}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>

@endsection
