@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <section class="panel">
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"><a
                                href="{{route('admin.dashboard.discounts.show',['id' => $discount->id])}}"> <i
                                    class="icon-user"></i> کد تخفیف</a></li>
                        <li><a href="{{route('admin.dashboard.discounts.edit',['id' => $discount->id])}}"> <i
                                    class="icon-user"></i>
                                ویرایش کد تخفیف</a></li>
                    </ul>
                </section>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <div class="bio-graph-heading">
                    مشخصات کد تخفیف
                </div>
                <div class="panel-body bio-graph-info">
                    <div class="row">
                        <div class="bio-row">
                            <p><span>کد :</span> {{$discount->id}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>عنوان :</span> {{$discount->title}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>درصد :</span> {{$discount->percent}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تعداد :</span> {{$discount->count}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>حداقل :</span> {{$discount->min}} تومان</p>
                        </div>
                        <div class="bio-row">
                            <p><span>حداکثر :</span> {{$discount->max}} تومان</p>
                        </div>
                        <div class="bio-row">
                            <p><span>حد مجاز هر کاربر :</span> {{$discount->per_user}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>وضعیت :</span> {{$discount->status}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ اعتبار :</span> {{$discount->expire_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ ثبت :</span> {{$discount->created_at_tehran}}</p>
                        </div>
                        <div class="bio-row">
                            <p><span>تاریخ بروزرسانی :</span> {{$discount->updated_at_tehran}}</p>
                        </div>

                    </div>
                </div>
            </section>
        </aside>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    فاکتور ها
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>ایدی</th>
                        <th>کاربر</th>
                        <th>قابل پرداخت</th>
                        <th>مبلغ کل</th>
                        <th>وضعیت</th>
                        <th>تاریخ ثبت</th>
                        <th>تاریخ بروزرسانی</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @foreach($discount->invoices as $invoice)
                        <tr class="gradeX odd">
                            <td>
                                <a href="{{route('admin.dashboard.invoices.show',['id'=>$invoice->id])}}">{{$invoice->id}}</a>
                            </td>
                            <td>
                                <a href="{{route('admin.dashboard.users.show',['id'=>$invoice->user->id])}}">{{$invoice->user->name}}{{$invoice->user->family}}</a>
                            </td>
                            <td>{{$invoice->payable_price}}</td>
                            <td>{{$invoice->total_price}}</td>
                            <td>{{$invoice->status}}</td>
                            <td>{{$invoice->created_at_tehran}}</td>
                            <td>{{$invoice->updated_at_tehran}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>

    </div>
@endsection
