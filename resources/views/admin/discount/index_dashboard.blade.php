@extends('admin.frame')
@section('custom-style')
    <script src="/js/dynamic-table.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    کد های تخفیف
                    <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-success">
                        +
                    </a>
                    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal"
                         class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×
                                    </button>
                                    <h4 class="modal-title">کد های تخفیف</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                                          action="{{route('admin.dashboard.discounts.store')}}" novalidate="novalidate">
                                        @csrf
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">کد</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="id"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">عنوان</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="title"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">درصد</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="title"
                                                       type="number" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">تعداد</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="count"
                                                       type="number" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">حداقل</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="min"
                                                       type="number" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">حداکثر</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="max"
                                                       type="number" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">حد مجاز هر کاربر</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="per_user"
                                                       type="number" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="cname" class="control-label col-lg-4">تاریخ انقضا</label>
                                            <div class="col-lg-12">
                                                <input class=" form-control"  name="expire_at"
                                                       type="text" required="">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <div class="col-lg-4">
                                                <button class="btn btn-danger" type="submit">ذخیره</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <table class="table table-striped border-top" id="sample_1">
                    <thead>
                    <tr>
                        <th>کد</th>
                        <th class="hidden-phone">عنوان</th>
                        <th class="hidden-phone">درصد</th>
                        <th class="hidden-phone">تاریخ انقضا</th>
                        <th class="hidden-phone">تاریخ ثبت</th>
                    </tr>
                    </thead>

                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                    @isset($discounts)
                        @foreach($discounts as $discount)
                            <tr class="gradeX odd">
                                <td class=" "><a
                                            href="{{route('admin.dashboard.discounts.show',['id' => $discount->id])}}">{{$discount->id}}</a>
                                </td>
                                <td class=" "><a
                                            href="{{route('admin.dashboard.discounts.show',['id' => $discount->id])}}">{{$discount->title}}</a>
                                </td>
                                <td class="center hidden-phone ">{{$discount->percent}}</td>
                                <td class="center hidden-phone ">{{$discount->expire_at_tehran}}</td>
                                <td class="center hidden-phone ">{{$discount->created_at_tehran}}</td>
                            </tr>
                        @endforeach
                    @endisset
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
