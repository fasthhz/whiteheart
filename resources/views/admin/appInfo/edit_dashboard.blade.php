@extends('admin.frame')
@section('custom-style')
    <script src="/js/form-validation-script.js"></script>
@endsection
@section('main-content')
    <div class="row">
        <aside class="profile-nav col-lg-3">
            <section class="panel">
                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="{{route('admin.dashboard.appInfo.show')}}">
                            <i class="icon-user"></i> اطلاعات سایت</a></li>
                    <li><a href="{{route('admin.dashboard.appInfo.edit')}}"> <i
                                class="icon-user"></i>
                            ویرایش اطلاعات</a></li>
                </ul>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    اسلایدر اصلی
                </div>
                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.appInfo.update')}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">متن</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="caption" minlength="1" type="text"
                                           required="" value=""></div>
                                <label class="col-lg-4 control-label">لینک</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="link" minlength="1" type="text"
                                           required="" value=""></div>
                                <label class="col-lg-4 control-label">عکس</label>
                                <div class="col-lg-8">
                                    <input type="file" name="image">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" name="addMainCarousel" value="true" class="btn btn-success">
                                        افزودن
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.appInfo.update')}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">اسلاید ها</label>
                                <div class="col-lg-8">
                                    <select name="carousel" multiple="" class="form-control">
                                        @foreach($info['mainCarousel'] as $carousel)
                                            <option value="{{$carousel['caption']}}">{{$carousel['caption']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" name="deleteMainCarousel" value="true" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">
                                        حذف
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    اسلایدر ثانویه
                </div>
                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.appInfo.update')}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">متن</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="caption" minlength="1" type="text"
                                           required="" value=""></div>
                                <label class="col-lg-4 control-label">لینک</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="link" minlength="1" type="text"
                                           required="" value=""></div>
                                <label class="col-lg-4 control-label">عکس</label>
                                <div class="col-lg-8">
                                    <input type="file" name="image">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" name="addSecondCarousel" value="true" class="btn btn-success">
                                        افزودن
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.appInfo.update')}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">اسلاید ها</label>
                                <div class="col-lg-8">
                                    <select name="carousel" multiple="" class="form-control">
                                        @foreach($info['secondCarousel'] as $carousel)
                                            <option value="{{$carousel['caption']}}">{{$carousel['caption']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" name="deleteSecondCarousel" value="true"
                                            type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">
                                        حذف
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    عکس صفحه اصلی
                </div>
                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.appInfo.update')}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">لینک</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="link" minlength="2" type="text"
                                           required="" value=""></div>
                                <label class="col-lg-4 control-label">عکس</label>
                                <div class="col-lg-8">
                                    <input type="file" name="image">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" name="addHomePic" value="true" class="btn btn-success">
                                        افزودن
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.appInfo.update')}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">عکس ها</label>
                                <div class="col-lg-8">
                                    <select name="image" multiple="" class="form-control">
                                        @foreach($info['homePics'] as $image)
                                            <option value="{{$image['image']}}">{{$image['image']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" name="deleteHomePic" value="true" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">
                                        حذف
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    لینک های مفید
                </div>
                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.appInfo.update')}}" method="POST"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">عنوان (فارسی)</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="title_fa" minlength="2" type="text"
                                           required="" value=""></div>
                                <label class="col-lg-4 control-label">عنوان (انگلیسی)</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="title_en" minlength="2" type="text"
                                           required="" value=""></div>
                                <label class="col-lg-4 control-label">عنوان (عربی)</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="title_ar" minlength="2" type="text"
                                           required="" value=""></div>
                                <label class="col-lg-4 control-label">لینک</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="link" minlength="2" type="text"
                                           required="" value=""></div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" name="addLink" value="true" class="btn btn-success">
                                        افزودن
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

                <div class="panel-body bio-graph-info">
                    <form class="form-horizontal" role="form"
                          action="{{route('admin.dashboard.appInfo.update')}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <div class="row">
                                <label class="col-lg-4 control-label">عکس ها</label>
                                <div class="col-lg-8">
                                    <select name="link" multiple="" class="form-control">
                                        @foreach($info['links'] as $link)
                                            <option value="{{$link['link']}}">{{$link['title_fa']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-danger" name="deleteLink" value="true" type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">
                                        حذف
                                    </button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </section>
        </aside>
        <aside class="profile-info col-lg-9">
            <section class="panel">
                <header class="panel-heading">
                    ویرایش اطلاعات
                </header>
                <div class="panel-body">
                    <div class=" form">
                        <form class="cmxform form-horizontal tasi-form" id="commentForm" method="POST"
                              action="{{route('admin.dashboard.appInfo.update')}}"
                              novalidate="novalidate" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group ">
                                <label class="col-lg-4 control-label">ادرس سایت</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="websiteUrl" minlength="2" type="text"
                                           required="" value="{{$info['websiteUrl']}}">
                                </div>
                                <label class="col-lg-4 control-label">ایمیل</label>
                                <div class="col-lg-8">
                                    <input class=" form-control" id="cname" name="email" minlength="2" type="text"
                                           required="" value="{{$info['email']}}">
                                </div>
                                <label class="col-lg-4 control-label">توسعه دهنده</label>
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-lg-4 ">
                                            <input class="col-lg-4 form-control" id="cname"
                                                   name="developerTitle"
                                                   minlength="2"
                                                   type="text"
                                                   required=""
                                                   value="{{$info['developedBy']['title']}}">
                                        </div>
                                        <div class="col-lg-4 ">
                                            <input class="col-lg-4 form-control" id="cname"
                                                   name="developerLink"
                                                   minlength="2"
                                                   type="text" required=""
                                                   value="{{$info['developedBy']['link']}}">
                                        </div>
                                        <div class="col-lg-4 ">
                                            <input class="col-lg-4 form-control" id="cname"
                                                   name="developerImage"
                                                   minlength="2"
                                                   type="text"
                                                   required=""
                                                   value="{{$info['developedBy']['image']}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-success" type="submit">ذخیره</button>
                                    <button class="btn btn-default" type="button">انصراف</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    شعب
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-9">
                            <form class="form-horizontal" role="form"
                                  action="{{route('admin.dashboard.appInfo.update')}}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label class="col-lg-4 control-label">عنوان</label>
                                            <div class="col-lg-8">
                                                <input class=" form-control" id="cname" name="title" minlength="2"
                                                       type="text"
                                                       required="" value=""></div>
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="col-lg-4 control-label">ادرس</label>
                                            <div class="col-lg-8">
                                                <textarea class="form-control" name="address" cols="60"
                                                          rows="5"></textarea></div>
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="col-lg-4 control-label">تلفن</label>
                                            <div class="col-lg-8">
                                                <textarea class="form-control" name="numbers" cols="60"
                                                          rows="5"></textarea></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button type="submit" name="addAgency" value="true"
                                                    class="btn btn-success">
                                                افزودن
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-3">
                            <form class="form-horizontal" role="form"
                                  action="{{route('admin.dashboard.appInfo.update')}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-lg-4 control-label">شعب</label>
                                        <div class="col-lg-8">
                                            <select name="agency" multiple="" class="form-control">
                                                @foreach($info['agencies'] as $agency)
                                                    <option
                                                        value="{{$agency['title']}}">{{$agency['title']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-danger" name="deleteAgency" value="true"
                                                    type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">
                                                حذف
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    شبکه های اجتماعی
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <form class="form-horizontal" role="form"
                                  action="{{route('admin.dashboard.appInfo.update')}}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-lg-4 control-label">عنوان</label>
                                        <div class="col-lg-8">
                                            <input class=" form-control" id="cname" name="title" minlength="2"
                                                   type="text"
                                                   required="" value=""></div>
                                        <label class="col-lg-4 control-label">لینک</label>
                                        <div class="col-lg-8">
                                            <input class=" form-control" id="cname" name="link" minlength="2"
                                                   type="text"
                                                   required="" value=""></div>
                                        <label class="col-lg-4 control-label">عکس</label>
                                        <div class="col-lg-8">
                                            <input type="file" name="image">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button type="submit" name="addSocialNetwork" value="true"
                                                    class="btn btn-success">
                                                افزودن
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                        <div class="col-lg-6">
                            <form class="form-horizontal" role="form"
                                  action="{{route('admin.dashboard.appInfo.update')}}" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-lg-4 control-label">شبکه ها</label>
                                        <div class="col-lg-8">
                                            <select name="socialNetwork" multiple="" class="form-control">
                                                @foreach($info['socialNetworks'] as $socialNetwork)
                                                    <option
                                                        value="{{$socialNetwork['title']}}">{{$socialNetwork['title']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-danger" name="deleteSocialNetwork" value="true"
                                                    type="submit" onclick="return confirm('اطلاعات بعد از پاک شده به هیچ عنوان قابل بازیابی نیستند. پاک شدن اطلاعات را تایید میکنید ؟')">
                                                حذف
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    درباره ما(Header)
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form class="form-horizontal" role="form"
                              action="{{route('admin.dashboard.appInfo.update')}}" method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                            <textarea class="form-control" name="fa" cols="60"
                                                      rows="5">{{$info['aboutUs']['header']['fa']}}</textarea></div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                            <textarea class="form-control" name="en" cols="60"
                                                      rows="5">{{$info['aboutUs']['header']['en']}}</textarea></div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                            <textarea class="form-control" name="ar" cols="60"
                                                      rows="5">{{$info['aboutUs']['header']['ar']}}</textarea></div>
                                </div>
                            </div>
                            <label class="col-lg-4 control-label">عکس :<a
                                    href="{{$info['aboutUs']['header']['image']}}">{{$info['aboutUs']['header']['image']}}</a>
                            </label>
                            <div class="col-lg-8">
                                <input type="file" name="image">
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" name="aboutUsHeader" value="true"
                                            class="btn btn-success">
                                        بروزرسانی
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    درباره ما(Title)
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form class="form-horizontal" role="form"
                              action="{{route('admin.dashboard.appInfo.update')}}" method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                            <textarea class="form-control" name="fa" cols="60"
                                                      rows="5">{{$info['aboutUs']['title']['fa']}}</textarea></div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                            <textarea class="form-control" name="en" cols="60"
                                                      rows="5">{{$info['aboutUs']['title']['en']}}</textarea></div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                            <textarea class="form-control" name="ar" cols="60"
                                                      rows="5">{{$info['aboutUs']['title']['ar']}}</textarea></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" name="aboutUsTitle" value="true"
                                            class="btn btn-success">
                                        بروزرسانی
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
            <section class="panel">
                <div class="bio-graph-heading">
                    درباره ما(Body)
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form class="form-horizontal" role="form"
                              action="{{route('admin.dashboard.appInfo.update')}}" method="POST"
                              enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                            <textarea class="form-control" name="fa" cols="60"
                                                      rows="5">{{$info['aboutUs']['body']['fa']}}</textarea></div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                            <textarea class="form-control" name="en" cols="60"
                                                      rows="5">{{$info['aboutUs']['body']['en']}}</textarea></div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                            <textarea class="form-control" name="ar" cols="60"
                                                      rows="5">{{$info['aboutUs']['body']['ar']}}</textarea></div>
                                </div>
                            </div>
                            <label class="col-lg-4 control-label">عکس :<a
                                    href="{{$info['aboutUs']['body']['image']}}">{{$info['aboutUs']['body']['image']}}</a>
                            </label>
                            <div class="col-lg-8">
                                <input type="file" name="image">
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" name="aboutUsBody" value="true"
                                            class="btn btn-success">
                                        بروزرسانی
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </aside>
    </div>
@endsection
