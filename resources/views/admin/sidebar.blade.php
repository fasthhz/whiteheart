<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
            <li class="{{isActive(['admin.dashboard.main'])}}">
                <a class="" href="{{route('admin.dashboard.main')}}">
                    <i class="icon-dashboard"></i>
                    <span>داشبورد</span>
                </a>
            </li>
            @can('users')

                <li class="sub-menu {{isActive(['admin.dashboard.users.search','admin.dashboard.users.index'])}}">
                    <a href="javascript:;" class="">
                        <i class="icon-group"></i>
                        <span>کاربران</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        <li><a class="" href="{{route('admin.dashboard.users.search')}}">جستجو</a></li>
                        <li><a class="" href="{{route('admin.dashboard.users.index')}}">لیست کاربران</a></li>
                    </ul>
                </li>
            @endcan

            @can('admins')

                <li class="sub-menu {{isActive(['admin.dashboard.admins.index','admin.dashboard.admins.create'])}}">
                    <a href="javascript:;" >
                        <i class="icon-user"></i>
                        <span>ادمین ها</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        <li><a class="" href="{{route('admin.dashboard.admins.create')}}">ثبت ادمین</a></li>
                        <li><a class="" href="{{route('admin.dashboard.admins.index')}}">لیست ادمین ها</a></li>
                    </ul>
                </li>
            @endcan

            @can('admins')

                <li class="sub-menu {{isActive(['admin.dashboard.branches.index','admin.dashboard.branches.create'])}}">
                    <a href="javascript:;" >
                        <i class="icon-user"></i>
                        <span>شعبه ها</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        <li><a class="" href="{{route('admin.dashboard.branches.create')}}">ثبت شعبه</a></li>
                        <li><a class="" href="{{route('admin.dashboard.branches.index')}}">لیست شعبه ها</a></li>
                    </ul>
                </li>
            @endcan

            @can('products')

                <li class="sub-menu {{isActive(['admin.dashboard.products.create','admin.dashboard.products.index'])}}">
                    <a href="javascript:;" class="">
                        <i class="icon-bullhorn"></i>
                        <span>محصولات</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        <li><a class="" href="{{route('admin.dashboard.products.create')}}">ثبت غذا</a>
                        </li>
                        <li><a class="" href="{{route('admin.dashboard.products.index')}}">لیست
                                غذا</a></li>
                    </ul>
                </li>
            @endcan
            @can('categories')
                <li class="sub-menu {{isActive(['admin.dashboard.categories.index','admin.dashboard.subcategories.index'])}}">
                    <a href="javascript:;" class="">
                        <i class="icon-book"></i>
                        <span>دسته بندی</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        <li><a class="" href="{{route('admin.dashboard.categories.index')}}">دسته</a>
                        </li>
                        <li><a class="" href="{{route('admin.dashboard.subcategories.index')}}">زیر
                                دسته</a></li>
                        {{--<li><a class="" href="{{route('admin.dashboard.festivals.index')}}">فستیوال</a>
                        </li>--}}
                        {{--<li><a class="" href="{{route('admin.dashboard.samples.index')}}">نمونه ها</a>
                        </li>--}}
                        {{--<li><a class="" href="{{route('admin.dashboard.nutrition.index')}}">مواد
                                غذایی</a></li>--}}
                    </ul>
                </li>
            @endcan
            {{-- @can('requests')
                 <li class="sub-menu">
                     <a href="javascript:;" class="">
                         <i class="icon-indent-right"></i>
                         <span>درخواست ها</span>
                         <span class="arrow"></span>
                     </a>
                     <ul class="sub">
                         <li><a class=""
                                href="{{route('admin.dashboard.sampleRequests.index')}}">نمونه</a></li>
                         <li><a class="" href="{{route('admin.dashboard.contactUs.index')}}">ارتباط</a>
                         </li>
                     </ul>
                 </li>
             @endcan--}}
            @can('invoices')

                <li class="{{isActive(['admin.dashboard.invoices.index'])}}">
                    <a class="" href="{{route('admin.dashboard.invoices.index')}}">
                        <i class="icon-shopping-cart"></i>
                        <span>سفارشات</span>
                    </a>
                </li>
            @endcan
            @can('couriers')

                <li class="{{isActive(['admin.dashboard.couriers.index'])}}">
                    <a class="" href="{{route('admin.dashboard.couriers.index')}}">
                        <i class="icon-rocket"></i>
                        <span>پیک ها</span>
                    </a>
                </li>
            @endcan
            @can('questions')

                <li class="{{isActive(['admin.dashboard.questions.index'])}}">
                    <a class="" href="{{route('admin.dashboard.questions.index')}}">
                        <i class="icon-question"></i>
                        <span>سوالات</span>
                    </a>
                </li>
            @endcan
            @can('comments')

                <li class="{{isActive(['admin.dashboard.comments.index'])}}">
                    <a class="" href="{{route('admin.dashboard.comments.index')}}">
                        <i class="icon-comment"></i>
                        <span>نظرات</span>
                    </a>
                </li>
            @endcan
            @can('settings')
                <li class="sub-menu {{isActive(['admin.dashboard.appInfo.show','admin.dashboard.deliveries.index','admin.dashboard.discounts.index'])}}">
                    <a href="javascript:;" class="">
                        <i class="icon-cogs"></i>
                        <span>تنظیمات</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub">
                        {{--<li><a class="" href="{{route('admin.dashboard.options.index')}}">ویژگی ها</a>
                        </li>--}}
                        <li><a class="" href="{{route('admin.dashboard.discounts.index')}}">کد تخفیف</a>
                        </li>
                        <li><a class="" href="{{route('admin.dashboard.deliveries.index')}}">روش های
                                ارسال</a></li>
                        <li><a class="" href="{{route('admin.dashboard.appInfo.show')}}">سایت</a></li>
                    </ul>
                </li>
            @endcan


        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
