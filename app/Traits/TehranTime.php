<?php
/**
 * Created by PhpStorm.
 * User: masih
 * Date: 8/6/19
 * Time: 7:52 PM
 */

namespace App\Traits;


use Morilog\Jalali\Jalalian;

trait TehranTime
{
    public function getCreatedAtTehranAttribute()
    {
        return Jalalian::fromCarbon($this->created_at->timezone('Asia/Tehran'));
    }

    public function getUpdatedAtTehranAttribute()
    {
        return Jalalian::fromCarbon($this->created_at->timezone('Asia/Tehran'));
    }

    public function getMobileVerifiedAtTehranAttribute()
    {
        return Jalalian::fromCarbon($this->mobile_verified_at->timezone('Asia/Tehran'));
    }

    public function getEmailVerifiedAtTehranAttribute()
    {
        return Jalalian::fromCarbon($this->email_verified_at->timezone('Asia/Tehran'));
    }

    public function getVerifiedAtTehranAttribute()
    {
        return Jalalian::fromCarbon($this->verified_at->timezone('Asia/Tehran'));
    }

    public function getExpireAtTehranAttribute()
    {
        return Jalalian::fromCarbon($this->expire_at->timezone('Asia/Tehran'));
    }
}