<?php

use App\Http\Resources\FalseTextResource;
use App\Http\Resources\TrueTextResource;
if(! function_exists('isActive')) {
        function isActive($key , $activeClassName= 'active'){
            if(is_array($key)){
                return in_array(Route::currentRouteName(), $key) ? $activeClassName : '';
            }

            return Route::currentRouteName() == $key ? $activeClassName : '';
        }
}

function trueJsonResponse(string $message, $code = null, $resource = null)
{
    if (!$code)
        $code = $resource == null ? 201 : 203;

    return new TrueTextResource($message, $code, $resource);
}


function falseJsonResponse(string $message, $errorCode = null, $data = null, $httpStatusCode = 400)
{
    if (!$errorCode)
        $errorCode = $data == null ? 401 : 403;
    return (new FalseTextResource($message, $errorCode, $data))->response()->setStatusCode($httpStatusCode);
}

function flash($message = NULL)
{
    $flash = new \App\Models\Flash();
    if ($message == NULL)
        return $flash;
    $flash->info($message);
}


function sms($receiver, $message, $sender = '10004346')
{
    return $result = Kavenegar::Send($sender, $receiver, $message);
}
