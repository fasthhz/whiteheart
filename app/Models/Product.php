<?php

namespace App\Models;

use App\Exceptions\NotEnoughQuantityException;
use App\Traits\TehranTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\UploadedFile;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model
{
    use TehranTime, HasSlug, SoftDeletes;


//    protected $with = [ 'branches' ];
    const BASE_IMAGE_PATH = '/products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'price', 'discount', 'status', 'quantity', 'priority', 'video'
    ];


    public function branches()
    {
        return $this->belongsToMany(Branch::class)
            ->withPivot(['price','discount','quantity','status']);
//            ->using(BranchProduct::class);
//            ->as('details');
    }

    /**
     * @return BelongsToMany
     */
    public function followers()
    {
        return $this->belongsToMany(User::class, 'user_favorite');
    }

    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class, 'subcategory_id');
    }


    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    public function items()
    {
        return $this->belongsToMany(Item::class, 'product_item');
    }


    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }


    public function invoices()
    {
        return $this->belongsToMany(Invoice::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function rates()
    {
        return $this->hasMany(Rate::class);
    }

    public function avgRates()
    {
        return $this->rates()->avg('rate');
    }

    public function getAvgRatesAttribute()
    {
        return $this->avgRates();
    }


    /**
     * @return BelongsToMany|HasMany
     */
    public function suggestions()
    {
        return $this->belongsToMany(Product::class, 'suggestions', 'product_id', 'suggested_id');
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    public function buyers()
    {
        return User::whereIn('id', $this->invoices()->pluck('user_id')->toArray());
    }

    public function getBuyersAttribute()
    {
        $buyers = $this->buyers()->get();
        $this->buyersId = $buyers->pluck('id')->toArray();
        return $buyers;
    }

    /**
     * @return BelongsToMany|HasMany
     */
    public function suggestedFor()
    {
        return $this->belongsToMany(Product::class, 'suggestions', 'suggested_id', 'product_id');
    }

    public function getRateAttribute()
    {
        return $this->comments->avg('rate');
    }

    public function inStock()
    {
        return $this->quantity > 0;
    }

    public function getInStockAttribute()
    {
        return $this->inStock();
    }

    /**
     * @param UploadedFile $image
     * @return false|Model
     */
    public function attacheUploadedImage(UploadedFile $image)
    {
        $image->storeAs(self::BASE_IMAGE_PATH, $image->hashName());
        return $this->images()->save(new Image(['original_name' => $image->getClientOriginalName(), 'hash_name' => $image->hashName()]));
    }


    /**
     * @param array $images
     * @return array
     */
    public function attacheUploadedImages(array $images)
    {
        $list = [];
        foreach ($images as $image)
            array_push($list, $this->attacheUploadedImage($image));
        return $list;
    }

    public function attachItems(array $ids)
    {
        $now = now();
        foreach ($ids as $id) {
            $this->items()->attach($id, ['created_at' => $now, 'updated_at' => $now]);
        }

    }

    /**
     * The $product can be Product object or product id
     * @param $product
     * @param $quantity
     * @return bool
     */
    public static function isValidForSale($product, $quantity)
    {
        if (!($product instanceof Product))
            $product = self::findOrFail($product);
        if (!$product->status)
            return false;
        if ($product->quantity < $quantity)
            return false;
        return true;
    }

    /**
     * The $list must be array of array
     * @param array $list
     * @return bool
     */
    public static function areValidForSale(array $list)
    {
        foreach ($list as $item)
            if (!self::isValidForSale($item[0], $item[1]))
                return false;
        return true;
    }

    /**
     * @param $str
     * @param array|null $columns
     * @return Product|Builder
     */
    public function scopeSearch($str, array $columns = null)
    {
        $products = self::with(['images', 'category']);
        $products = $products->where('title', 'like', "%$str%");
        $products = $products->where('description', 'like', "%$str%");
        return $products;
    }

    /**
     * Find and sub count of product quantity and return it
     * @param $qty
     * @return mixed
     * @throws \Throwable
     */
    public function buy($qty)
    {
        if($this->quantity < $qty)
            throw new NotEnoughQuantityException($this);
        $this->quantity -= $qty;
        return $this->update();
    }

    public function forceDelete()
    {
        foreach ($this->images as $image)
            $image->delete();
        return parent::forceDelete();
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->usingLanguage(false);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopeFindBySlug($query, String $slug)
    {
        return $query->where('slug', $slug)->firstOrFail();
    }

    public function scopeFindByIdOrSlugOrFail($query, String $category)
    {
        return $query
            ->whereRaw("(id = ? or slug = ?)", [$category, $category])
            ->firstOrFail();
    }

    public function getPriceAttribute()
    {
//        $prices = collect([]);
//        foreach ($this->branches()->get() as $key => $branch) {
//            $prices->put($key,[
//                'price' => $this->branches()->get()->flatten()[$key]->pivot->price,
//                'quantity' => $this->branches()->get()->flatten()[$key]->pivot->quantity,
//                'status' => $this->branches()->get()->flatten()[$key]->pivot->status ? true : false,
//                'product_id' => $this->branches()->get()->flatten()[$key]->pivot->product_id,
//                'branch_id' => $this->branches()->get()->flatten()[$key ]->pivot->branch_id
//            ]);
//        }
//        return $this->branches()->find(request()->branch_id)->pivot->price;
//        return $prices->min('price').' تا '.$prices->max('price');
    }

    public function getFinalPriceAttribute()
    {
        $prices = collect([]);
        foreach ($this->branches()->get() as $key => $branch) {
            $prices->put($key,[
                'price' => $this->branches()->get()->flatten()[$key]->pivot->price,
                'quantity' => $this->branches()->get()->flatten()[$key]->pivot->quantity,
                'status' => $this->branches()->get()->flatten()[$key]->pivot->status ? true : false,
                'product_id' => $this->branches()->get()->flatten()[$key]->pivot->product_id,
                'branch_id' => $this->branches()->get()->flatten()[$key ]->pivot->branch_id
            ]);
        }
        return $prices->max('price');
        // return $prices->max('price')*($prices->max('discount')??1)/100;
//        return $prices->min('price').' تا '.$prices->max('price');
    }

    public function getQuantityAttribute()
    {
        $prices = collect([]);
        foreach ($this->branches()->get() as $key => $branch) {
            $prices->put($key,[
                'price' => $this->branches()->get()->flatten()[$key]->pivot->price,
                'quantity' => $this->branches()->get()->flatten()[$key]->pivot->quantity,
                'status' => $this->branches()->get()->flatten()[$key]->pivot->status ? true : false,
                'product_id' => $this->branches()->get()->flatten()[$key]->pivot->product_id,
                'branch_id' => $this->branches()->get()->flatten()[$key ]->pivot->branch_id
            ]);
        }
        return $prices->sum('quantity');
//        return $prices->min('price').' تا '.$prices->max('price');
    }

    public function getStatusAttribute()
    {
        $prices = collect([]);
        foreach ($this->branches()->get() as $key => $branch) {
            $prices->put($key,[
                'price' => $this->branches()->get()->flatten()[$key]->pivot->price,
                'quantity' => $this->branches()->get()->flatten()[$key]->pivot->quantity,
                'status' => $this->branches()->get()->flatten()[$key]->pivot->status ? true : false,
                'product_id' => $this->branches()->get()->flatten()[$key]->pivot->product_id,
                'branch_id' => $this->branches()->get()->flatten()[$key ]->pivot->branch_id
            ]);
        }
        return true;
    }

    public function scopePriceOfBranch($query, $branch_id)
    {
        if(!empty($this->branches()->find(request()->branch_id)->pivot->price))
        {
            dd($this->branches()->find(request()->branch_id)->pivot->price);
            return $this->branches()->find(request()->branch_id)->pivot->price;
        }
        return '';
    }
}
