<?php

namespace App\Models;

use App\Traits\TehranTime;
use Exception;
use Gateway;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Larabookir\Gateway\Zarinpal\Zarinpal;

class Transaction extends Model
{
    use TehranTime;
    protected $table = 'gateway_transactions';
    protected $fillable = ['transactionable_type','transactionable_id'];

    /**
     * @return MorphTo
     */
    public function transactionable()
    {
        return $this->morphTo('transactionable');
    }

    public function scopePayInvoice($query, Invoice $invoice)
    {
        $gateway = Gateway::make(new Zarinpal());
        $gateway
            ->price($invoice->payable_price * 10)
            ->ready();
        $refId = $gateway->refId(); // شماره ارجاع بانک
        $transID = $gateway->transactionId(); // شماره تراکنش
        // save invoice id in transaction record
        Transaction::findOrFail($transID)->update(['transactionable_id' => $invoice->id, 'transactionable_type' => 'App\Models\Invoice']);
        return $gateway;
    }
}
