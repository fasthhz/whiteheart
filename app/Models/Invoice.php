<?php

namespace App\Models;

use App\Traits\TehranTime;
use Eloquent;
use Exception;
use Gateway;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Larabookir\Gateway\Zarinpal\Zarinpal;

class Invoice extends Model
{
    use TehranTime;


    protected $fillable = ['address', 'description', 'paid', 'branch_id'];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * @return BelongsTo
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
    /**
     * @return BelongsTo
     */
    public function user_address()
    {
        return $this->belongsTo(UserAddress::class);
    }

    /**
     * @return BelongsTo
     */
    public function courier()
    {
        return $this->belongsTo(Courier::class);
    }

    /**
     * @return BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot(['count']);
    }

    /**
     * @return BelongsTo
     */
    public function discount()
    {
        return $this->belongsTo(Discount::class);
    }

    /**
     * @return BelongsTo
     */
    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }

    /**
     * Return all transactions that happened for this invoice
     * @return MorphMany
     */
    public function transactions()
    {
        return $this->morphMany('App\Models\Transaction', 'transactionable');
    }

    public function payWithCreditCard()
    {
        if ($this->paid != 'None')
            return false;
        return Transaction::payInvoice($this);
    }

    public function payingWithCreditCard()
    {
        if ($this->paid != 'None')
            return false;
        return $this->update(['paid' => 'Card']);
    }

    public function payWithCash()
    {
        if ($this->paid != 'None')
            return false;
        return $this->update(['paid' => 'Cash']);
    }

    public function useDiscount(Discount $discount)
    {
        $discount->validFor($this->order_price, $this->user);
        $this->discount_id = $discount->id;
        $this->payable_price = $this->payable_price * (100 - $discount->percent) / 100;
    }

    public function setDelivery(Delivery $delivery)
    {
        $this->total_price += $delivery->price;
        $this->payable_price += $delivery->price;
    }

    public function addItemWithOutSave(Product $product, $qty = 1)
    {
        $this->total_price += $qty * $product->price;
        $this->payable_price += $qty * $product->price * (100 - $product->discount) / 100;
        $this->orders_price += $qty * $product->price;
    }

    public function getStatusAttribute($value)
    {
        switch ($value) {
            case(0):
                return "ثبت شده";
                break;
            case(1):
                return "درحال اماده سازی";
                break;
            case(2):
                return "ارسال شده";
                break;
            case(3):
                return " رد شده";
                break;
        }
    }
}
