<?php

namespace App\Models;

use App\Models\Image;
use App\Models\Product;
use App\Traits\TehranTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;


class Festival extends Model
{
    use TehranTime,HasSlug;
    const BASE_IMAGE_PATH = '/festival';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description'
    ];

    /**
     * @return BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * @return MorphOne
     */
    public function image()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }

    /**
     * @param UploadedFile $image
     * @return false|Model
     */
    public function attacheUploadedImage(UploadedFile $image)
    {
        $image->storeAs(self::BASE_IMAGE_PATH, $image->hashName());
        return $this->image()->save(new Image(['original_name' => $image->getClientOriginalName(), 'hash_name' => $image->hashName()]));
    }

    /**
     * @return bool|null
     * @throws \Exception
     */
    public function delete()
    {
        $this->image->delete();
        return parent::delete();
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->usingLanguage(false);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopeFindBySlug($query,String $slug)
    {
        return $query->where('slug',$slug)->firstOrFail();
    }

    public function scopeFindByIdOrSlugOrFail($query, String $category)
    {
        return $query
            ->whereRaw("(id = ? or slug = ?)",[$category,$category])
            ->firstOrFail();
    }
}
