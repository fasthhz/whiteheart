<?php

namespace App\Models;

use App\Traits\TehranTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
//    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use TehranTime;

    protected const MAX_VERIFYING_CODE_RANGE = 1000;
    protected const MIN_VERIFYING_CODE_RANGE = 9999;
    protected const MAX_VERIFYING_CODE_EXPIRE_TIME = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'family',
        'mobile',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'mobile_verified_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function routeNotificationForKavenegar($driver, $notification = null)
    {
        return $this->mobile;
    }

    public function addresses()
    {
        return $this->hasMany(UserAddress::class);
    }


    public function favorites()
    {
        return $this->belongsToMany(Product::class, 'user_favorite');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function sms(string $text)
    {
        return sms($this->mobile, $text);
    }

    public function getTransactions()
    {
        return $this->invoices->pluck('transactions')->collapse();
    }

    public function verifyMobile()
    {
        $this->mobile_verified_at = now();
        return $this->update();
    }


    public function generateVerifyingCode()
    {
        $code = rand(self::MIN_VERIFYING_CODE_RANGE, self::MAX_VERIFYING_CODE_RANGE);
        Cache::put("MobileVerifyCode|{$this->mobile}", $code, now()->addMinutes(self::MAX_VERIFYING_CODE_EXPIRE_TIME));
        return $code;
    }

    public function checkVerifyingCode($code)
    {
        $generatedCode = Cache::get("MobileVerifyCode|{$this->mobile}");
        return isset($generatedCode) && $generatedCode == $code;
    }

    public static function createOrUpdateWhenMobileNotVerified(array $attributes)
    {
        try {
            $user = User::where('mobile', $attributes['mobile'])->firstOrFail();
            if ($user->mobile_verified_at)
                return false;
            $user->password = bcrypt($attributes['password']);
            $user->update($attributes);
            return $user;
        } catch (ModelNotFoundException $exception) {
            $user = new User($attributes);
            $user->mobile = $attributes['mobile'];
            $user->password = bcrypt($attributes['password']);
            $user->save();
            return $user;
        }

    }


}
