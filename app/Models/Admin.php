<?php

namespace App\Models;

use App\Traits\TehranTime;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Spatie\Permission\Traits\HasRoles;


class Admin extends Authenticatable
{
    protected $fillable =['name','family','username'];
    use TehranTime , HasRoles;

    public function activities()
    {
        return $this->morphMany('Spatie\Activitylog\Models\Activity','activity_log','causer_type','causer_id');
    }
}
