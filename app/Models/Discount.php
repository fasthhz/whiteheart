<?php

namespace App\Models;

use App\Exceptions\DiscountNotValid;
use App\Traits\TehranTime;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

class Discount extends Model
{
    use TehranTime;
    protected $keyType = 'string';
    protected $fillable = ['id', 'title', 'percent', 'per_user', 'status', 'count', 'code', 'min', 'max', 'expire_at'];

    protected $casts = [
        'expire_at' => 'datetime',
    ];

    /**
     * @return HasMany
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function users()
    {
        return $this->hasManyThrough(User::class, Invoice::class);
    }

    /**
     * @param Invoice $invoice
     * @param User $user
     * @return Discount
     * @throws DiscountNotValid
     */
    public function useFor(Invoice $invoice, User $user)
    {
        if (!$this->validFor($invoice->order_price, $user))
            throw new DiscountNotValid();
        $this->count -= 1;
        $this->updateOrFail();
        return $this;
    }

    public function validFor($price, User $user)
    {
        if ($this->count < 1)
            return false;
        elseif ($this->status < 0)
            return false;
        elseif ($this->expire_at < now())
            return false;
        elseif ($this->invoices()->where('user_id', $user->id)->where('discount_id', $this->id)->count() >= $this->per_user)
            return false;
        elseif ($price > $this->max || $price < $this->min)
            return false;
        return true;
    }

    public function scopeFindByCode($query, $code)
    {
        return $query->where('code', $code)->first();
    }

    public function scopeFindByCodeOrFail($query, $code)
    {
        return $query->where('code', $code)->firstOrFail();
    }
}
