<?php

namespace App\Models;

use App\Traits\TehranTime;
use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    use TehranTime;
    protected $fillable=['name','email','mobile','message'];
}
