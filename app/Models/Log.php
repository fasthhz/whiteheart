<?php


namespace App\Models;


use App\Traits\TehranTime;
use Spatie\Activitylog\Models\Activity;

class Log extends Activity
{
    use TehranTime;
    protected $table = 'activity_log';
}
