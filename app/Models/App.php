<?php


namespace App\Models;


use Illuminate\Support\Facades\Storage;

class App
{
    const BASE_IMAGE_PATH = '/app';
    private $filePath = 'app/info';
    private $file;
    private static $application;
    /**
     * Attributes of application
     * @var array
     */
    private $attributes;

    private function __construct()
    {
        $this->file = Storage::get($this->filePath);
        $this->attributes = json_decode($this->file, true);
        self::$application = $this;
    }

    public static function info($key = null)
    {
        isset(self::$application) ?: new self();
        if ($key != null)
            return self::$application->attributes[$key];

        return self::$application->attributes;
    }

    public static function update(array $info)
    {
        if (self::$application == null)
            self::info();
        foreach ($info as $key => $value)
            self::$application->setAttribute($key, $value);
        return self::$application->saveToFile();
    }

    private function setAttribute(string $key, $value)
    {
        if (array_key_exists($key, $this->attributes))
            $this->attributes[$key] = $value;

    }

    private function saveToFile()
    {
        return !!Storage::put($this->filePath, json_encode($this->attributes));
    }

    public static function addMainCarousel(array $carousel)
    {
        $image = Image::saveUploadedImage($carousel['image'], self::BASE_IMAGE_PATH);
        $carousel = ['caption' => $carousel['caption'], 'image' => route('images.show', ['id' => $image->id]), 'link' => $carousel['link']];
        $carousels = self::info('mainCarousel');
        array_push($carousels, $carousel);
        return self::update(['mainCarousel' => $carousels]);
    }

    public static function deleteMainCarousel($caption)
    {
        $carousel = self::info('mainCarousel');
        $newCarousel = [];
        foreach ($carousel as $item) {
            if ($item['caption'] == $caption) {
                Image::findByLink($item['image'])->delete(self::BASE_IMAGE_PATH);
                continue;
            }
            array_push($newCarousel, $item);
        }
        return self::update(['mainCarousel' => $newCarousel]);
    }

    public static function addSecondCarousel(array $carousel)
    {
        $image = Image::saveUploadedImage($carousel['image'], self::BASE_IMAGE_PATH);
        $carousel = ['caption' => $carousel['caption'], 'image' => route('images.show', ['id' => $image->id]), 'link' => $carousel['link']];
        $carousels = self::info('secondCarousel');
        array_push($carousels, $carousel);
        return self::update(['secondCarousel' => $carousels]);
    }

    public static function deleteSecondCarousel($caption)
    {
        $carousel = self::info('secondCarousel');
        $newCarousel = [];
        foreach ($carousel as $item) {
            if ($item['caption'] == $caption) {
                Image::findByLink($item['image'])->delete(self::BASE_IMAGE_PATH);
                continue;
            }
            array_push($newCarousel, $item);
        }
        return self::update(['secondCarousel' => $newCarousel]);
    }

    public static function addAgency(array $agency)
    {
        $agency = ['title' => $agency['title'], 'numbers' => $agency['numbers'], 'address' => $agency['address']];
        $agencies = self::info('agencies');
        array_push($agencies, $agency);
        return self::update(['agencies' => $agencies]);
    }

    public static function deleteAgency($title)
    {
        $agencies = self::info('agencies');
        $newAgencies = [];
        foreach ($agencies as $agency) {
            if ($agency['title'] != $title)
                array_push($newAgencies, $agency);
            return self::update(['agencies' => $newAgencies]);
        }
    }

    public static function addSocialNetwork(array $agency)
    {
        $image = Image::saveUploadedImage($agency['image'], self::BASE_IMAGE_PATH);
        $socialNetwork = ['title' => $agency['title'], 'image' => route('images.show', ['id' => $image->id]), 'link' => $agency['link']];
        $socialNetworks = self::info('socialNetworks');
        array_push($socialNetworks, $socialNetwork);
        return self::update(['socialNetworks' => $socialNetworks]);
    }

    public static function deleteSocialNetwork($title)
    {
        $socialNetworks = self::info('socialNetworks');
        $newSocialNetworks = [];
        foreach ($socialNetworks as $socialNetwork) {
            if ($socialNetwork['title'] == $title) {
                Image::findByLink($socialNetwork['image'])->delete(self::BASE_IMAGE_PATH);
                continue;
            }
            array_push($newSocialNetworks, $socialNetwork);
        }
        return self::update(['socialNetworks' => $newSocialNetworks]);
    }

    public static function updateAboutUsHeader(array $info)
    {
        $aboutUs = self::info('aboutUs');
        $image = $aboutUs['header']['image'];
        if (isset($info['image']))
            $image = route('images.show', ['id' => Image::saveUploadedImage($info['image'], self::BASE_IMAGE_PATH)->id]);
        $aboutUs['header'] = ['fa' => $info['fa'], 'en' => $info['en'], 'ar' => $info['ar'], 'image' => $image];
        return self::update(['aboutUs' => $aboutUs]);
    }

    public static function updateAboutUsTitle(array $info)
    {
        $aboutUs = self::info('aboutUs');
        $aboutUs['title'] = ['fa' => $info['fa'], 'en' => $info['en'], 'ar' => $info['ar']];
        return self::update(['aboutUs' => $aboutUs]);
    }

    public static function updateAboutUsBody(array $info)
    {
        $aboutUs = self::info('aboutUs');
        $image = $aboutUs['body']['image'];
        if (isset($info['image']))
            $image = route('images.show', ['id' => Image::saveUploadedImage($info['image'], self::BASE_IMAGE_PATH)->id]);
        $aboutUs['body'] = ['fa' => $info['fa'], 'en' => $info['en'], 'ar' => $info['ar'], 'image' => $image];
        return self::update(['aboutUs' => $aboutUs]);
    }

    public static function addHomePic($image,$link)
    {
        $image = ['image' => route('images.show', ['id' => Image::saveUploadedImage($image, self::BASE_IMAGE_PATH)->id]), 'link' => $link];
        $homePics = self::info('homePics');
        array_push($homePics, $image);
        return self::update(['homePics' => $homePics]);
    }

    public static function deleteHomePic($image)
    {
        $homePics = self::info('homePics');
        $newHomePics = [];
        foreach ($homePics as $homePic) {
            if ($homePic['image'] == $image) {
                Image::findByLink($image)->delete(self::BASE_IMAGE_PATH);
                continue;
            }
            array_push($newHomePics, $homePic);
        }
        return self::update(['homePics' => $newHomePics]);
    }
    public static function addLink(array $info)
    {
        $link = ['title_fa' => $info['title_fa'],'title_en' => $info['title_en'],'title_ar' => $info['title_ar'], 'link' => $info['link']];
        $links = self::info('links');
        array_push($links, $link);
        return self::update(['links' => $links]);
    }

    public static function deleteLink($url)
    {
        $links = self::info('links');
        $newlinks = [];
        foreach ($links as $link) {
            if ($link['link'] == $url)
                continue;
            array_push($newlinks, $link);
        }
        return self::update(['links' => $newlinks]);
    }
}
