<?php

namespace App\Models;

use App\Traits\TehranTime;
use Eloquent;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;


class Image extends Model
{

    use TehranTime;
    const BASE_IMAGE_PATH = '/app';

    protected $fillable = ['original_name', 'hash_name'];

    /**
     * @return MorphTo
     */
    public function imageable()
    {
        return $this->morphTo('imageable');
    }

    /**
     * @return string
     */
    public function getImage()
    {
        $model = $this->imageable()->getModel();
        return Storage::get($model::BASE_IMAGE_PATH . '/' . $this->hash_name);
    }

    public function download()
    {
        $model = $this->imageable()->getModel();
        return Storage::download($model::BASE_IMAGE_PATH . '/' . $this->hash_name, $this->original_name);
    }

    public function delete($path = null)
    {
        $model = $this->imageable()->getModel();
        $path = $path == null ? $model::BASE_IMAGE_PATH : $path;
        Storage::delete($path . '/' . $this->hash_name);
        return parent::delete();
    }

    /**
     * @param UploadedFile $image
     * @param null $path
     * @return Image|bool
     */
    public function scopeSaveUploadedImage($query, UploadedFile $image, $path = null)
    {
        $image->storeAs($path, $image->hashName());
        $model = new Image(['original_name' => $image->getClientOriginalName(), 'hash_name' => $image->hashName()]);
        return $model->save() ? $model : false;
    }

    public function scopeFindByLink($query, string $link)
    {
        $imageId = explode('/', $link)[count(explode('/', $link)) - 1];
        return self::find($imageId);
    }
}
