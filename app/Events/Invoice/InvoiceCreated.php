<?php

namespace App\Events\Invoice;

use App\Events\CreatedEvent;
use App\Models\Admin;
use App\Models\Invoice;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Database\Eloquent\Model;

class InvoiceCreated extends CreatedEvent implements ShouldBroadcast
{
    public $invoice;
    public $creator;
    public $message;
    public $type;
    public $url;
    /**
     * AdminUpdated constructor.
     * @param Invoice $invoice
     * @param string $description
     */
    public function __construct(Invoice $invoice, string $description = "")
    {
        $this->message ="سفارش جدید";
        $this->type ="success";
        $this->url =route('admin.dashboard.invoices.show',[$invoice->id]);
        $this->invoice = $invoice;
        $this->creator = $invoice->user;
        parent::__construct($invoice, $this->creator , 'invoice created', $description);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return ['admin_notification'];
    }

}
