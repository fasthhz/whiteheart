<?php

namespace App\Events\Invoice;

use App\Events\UpdatedEvent;
use App\Models\Admin;
use App\Models\Invoice;
use Illuminate\Database\Eloquent\Model;

class InvoiceUpdated extends UpdatedEvent
{
    public $invoice;
    public $admin;

    /**
     * AdminUpdated constructor.
     * @param Invoice $invoice
     * @param array $before
     * @param Admin $admin
     * @param string $description
     */
    public function __construct(Invoice $invoice, array $before, Admin $admin, string $description = "")
    {
        $this->invoice = $invoice;
        $this->admin = $admin;
        parent::__construct($invoice, $before, $admin, 'invoice updated', $description);
    }


}
