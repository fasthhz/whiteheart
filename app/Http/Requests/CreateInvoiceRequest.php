<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class CreateInvoiceRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'products' => 'json|required',
            'address' => 'numeric|required|exists:user_addresses,id',
            'description' => 'string',
            'discount' => 'string',

        ];
    }

    public function messages()
    {
        return [];
    }

    public function products()
    {
        return json_decode($this->input('products'), true);
    }
}
