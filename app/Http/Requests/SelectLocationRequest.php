<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class SelectLocationRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "api_token"=> '',
            "city"=> 'string|required',
            "device"=> 'string|required|max:5',
            "lang"=> 'string|required|max:2',
            "lat_user"=> 'numeric|required',
            "lng_user"=> 'numeric|required',
            "package"=> '',
            "phone_unique_code"=> '',
            "user_temp_token"=> '',
            "user_token"=> '',
        ];
    }

}
