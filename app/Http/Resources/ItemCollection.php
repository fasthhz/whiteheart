<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ItemCollection extends BaseCollection
{
    public $collects = 'App\Http\Resources\ItemResource';
}
