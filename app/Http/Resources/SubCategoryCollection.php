<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class SubCategoryCollection extends BaseCollection
{
    public $collects = 'App\Http\Resources\SubCategoryResource';
}
