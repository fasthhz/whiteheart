<?php

namespace App\Http\Resources;

use App\Models\Alternative;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class VariantResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'alternatives' => AlternativeResource::collection($this->whenLoaded('alternatives')),
            'products' => ProductResource::collection($this->whenLoaded('products')),
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at
        ];
    }
}
