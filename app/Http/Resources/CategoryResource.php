<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'description' => $this->description,
            'image' => ImageResource::make($this->whenLoaded('image')),
            'subcategories' => SubCategoryResource::collection($this->whenLoaded('subcategories')),
            'products' => ProductResource::collection($this->whenLoaded('products')),
        ];
    }
}
