<?php

namespace App\Http\Responses;

use App\Models\User;
use App\Notifications\SendVerifyCode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{

public function toResponse($request)
{
    $user = Auth::user();
    if($user->mobile_verified_at==null){
        if( Cache::get("MobileVerifyCode|{$user->mobile}") ==null){
            $user->notify(new SendVerifyCode($request->user()));
        }
        return redirect()->route('verifyMobile',$user->id);
    }
    else{
        if( Cache::get("MobileVerifyCode|{$user->mobile}") == null){
            $user->notify(new SendVerifyCode($request->user()));
        }
        return redirect()->route('login');
    }
    //    dd($request);
//
//// below is the existing response
//// replace this with your own code
//// the user can be located with Auth facade
//$role = Auth::user()->role;
//$checkrole = explode(',', $role);  // However you get role
//
//if (in_array('admin', $checkrole)) {
//Session::put('isadmin', 'admin');
//return redirect('dog/indexadmin');
//} else {
//return redirect('pet/index');
//}


    return $request->wantsJson()
    ? response()->json(['two_factor' => false])
    : redirect()->intended(config('fortify.home'));
}

}
