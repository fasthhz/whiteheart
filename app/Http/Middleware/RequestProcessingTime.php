<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RequestProcessingTime
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $next = $next($request);
        $processingTime = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        $next->headers->set('Request-Processing-Time',$processingTime);
        return $next;
    }
}
