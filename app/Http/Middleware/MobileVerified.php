<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class MobileVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,  $redirectToRoute = null)
    {
        if(Auth::check()) {
                if (!$request->user() ||
                    $request->user()->mobile_verified_at == null) {
                    return $request->expectsJson()
                        ? abort(403, 'موبایل شما تایید نشده است.')
                        : Redirect::guest(URL::route($redirectToRoute ?: 'verifyMobile', $request->user()->id));
                }
        }
        return $next($request);
    }
}
