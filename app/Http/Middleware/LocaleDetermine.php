<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LocaleDetermine
{
    public $baseLang = "en";

    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header("Accept-Language"))
            App::setLocale($this->determine($request->header("Accept-Language"))?:$this->baseLang);
        return $next($request);
    }

    protected function determine(string $header)
    {
        $languages = explode(',', $header);
        return explode(';',$languages[0])[0];
    }
}
