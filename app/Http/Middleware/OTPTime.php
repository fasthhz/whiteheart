<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Notifications\SendVerifyCode;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class OTPTime
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,  $redirectToRoute = null)
    {
        if(!Auth::check()) {
                    if(Session::get('user_login_id') && Session::get('login-step')==2) {
                        $user = User::query()->find(Session::get('user_login_id'));
                        $generatedCode = Cache::get("MobileVerifyCode|{$user->mobile}");
                        if(isset($generatedCode)){
                            return \redirect()->back()->withErrors(['message'=>'کد قبلی معتبر است و میتوانید استفاده کنید']);
                        }
                        return $request->expectsJson()
                            ? abort(403, 'مدت زمان گذشته برای ارسال مجدد کد تایید سپری نشده است!')
                            : Redirect::guest(URL::route($redirectToRoute ?: 'login'));
                    }
        }
        return $next($request);
    }
}
