<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class AuthController extends Controller
{

    public function __construct()
    {

    }

    public function verifyMobile($id)
    {
        $user = User::query()->find($id);
        return view('auth.verify-mobile' , compact('user'));
    }

    public function verifyMobileProcess(Request $request,$id)
    {
        $user = User::query()->find($id);
        $code = $request->code;
        $generatedCode = Cache::get("MobileVerifyCode|{$user->mobile}");
        $status =  isset($generatedCode) && $generatedCode == $code;
        if($status==true){
            $user->verifyMobile();
            return redirect()->route('home');
        }
        abort(403, 'کد وارد شده اشتباه است.');
    }
}
