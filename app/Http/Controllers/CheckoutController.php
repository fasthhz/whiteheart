<?php

namespace App\Http\Controllers;

use App\Events\Invoice\InvoiceCreated;
use App\Models\Branch;
use App\Models\Invoice;
use App\Models\Transaction;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Larabookir\Gateway\Gateway;

class CheckoutController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:web');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkout()
    {
        $items = Session::get('cartItems');
        $discount = null;
        $directGet = \request()->get('directGet');
        $branch = \request()->get('branch_id');
        if(isset($directGet) && $directGet==1){
            $branch = Branch::query()->find($branch);
        }
        if (!$branch && $directGet!=null){
            echo 'چنین شعبه ای وجود ندارد';
            die();
        }
        $total = Collection::make($items)->sum('price');
        return view('checkout',compact(['items','discount','total','branch']));
    }

    public function pay(Request $request)
    {
        $pay = null;
         $request->validate([
            'address'  => 'required'
        ]);
        DB::beginTransaction();
        $newAddress = false;
        $cartItems = Collection::make(Session::get('cartItems'));
        $address = UserAddress::query()->find($request->input('address'));
        if(!$address){
            $address = $request->input('address');
        }
        if(isset($request->address) && $request->newAddress!=null){
            $newAddress = true;
            $address = Auth::user()->addresses()->create([
                'name'=>$request->name,
                'family'=>$request->family,
                'mobile'=>$request->mobile,
                'phone'=>$request->phone,
                'address' => $request->newAddress,
                'branch_id'=> ($request->has('branch_id')?$request->get('branch_id'):1)
            ]);
        }
        if($request->paymentMethod=='cod'){
            $invoice = Auth::user()->invoices()->create([
                "courier_id"=>null,
                "discount_id"=>null,
                "status"=>1,
                "payable_price"=>$cartItems->sum('price'),
                "orders_price"=>$cartItems->sum('price'),
                "total_price"=>$cartItems->sum('price'),
                "address"=>($request->directGet==1&&$request->branch_id?Branch::query()->find($request->branch_id)->address:$address->forInvoice()),
                "paid"=>"Cash",
                "description"=>null,
                "get_ready_at"=>null,
                "branch_id"=>$request->branch_id
            ]);
        }
        else {
            $invoice = Auth::user()->invoices()->create([
                "courier_id"=>null,
                "discount_id"=>null,
                "status"=>1,
                "payable_price"=>$cartItems->sum('price'),
                "orders_price"=>$cartItems->sum('price'),
                "total_price"=>$cartItems->sum('price'),
                "address"=>($request->directGet==1&&$request->branch_id?Branch::query()->find($request->branch_id)->address:$address),
                "paid"=>"Card",
                "description"=>null,
                "get_ready_at"=>null,
                "branch_id"=>$request->branch_id
            ]);

            $pay = 'Card';
        }

        $products = $invoice->products()->attach($cartItems->pluck('id')->toArray(),['count' => $cartItems->count()]);
        event(new InvoiceCreated($invoice));

        if (!$invoice->update()) {
            DB::rollBack();
            return redirect()->back()->withErrors('عملیات با شکست مواجه شد!');
        }
        DB::commit();
        Session::pull('cartItems');
if($pay){
    $gateway = Gateway::make('zarinpal');

//                $gateway->setCallback(url('/bank/response')); // You can also change the callback
    $gateway->price($invoice->payable_price * 10)
        // setShipmentPrice(10) // optional - just for paypal
        // setProductName("My Product") // optional - just for paypal
        ->ready();

    $refId =  $gateway->refId(); // شماره ارجاع بانک
    $transID = $gateway->transactionId(); // شماره تراکنش

    // در اینجا
    //  شماره تراکنش  بانک را با توجه به نوع ساختار دیتابیس تان
    //  در جداول مورد نیاز و بسته به نیاز سیستم تان
    // ذخیره کنید .

    return $gateway->redirect();

}

//        return redirect()->route('checkout.after_pay');
    }

    public function thank_you(Request $request)
    {
        return view('thank-you');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
