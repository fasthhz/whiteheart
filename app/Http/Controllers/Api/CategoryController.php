<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductCollection;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return CategoryCollection
     */
    public function index(Request $request)
    {
        return new CategoryCollection(Category::with(['image', 'products' => function ($query) {
            $query->where('status',1)->orderByDesc('priority')->with('images');
        }])->orderByDesc('priority')->get());
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return CategoryResource
     */
    public function show($id)
    {
        try {
            $category = Category::with(['subcategories.image', 'image'])->findByIdOrSlugOrFail($id);
            return new CategoryResource($category);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.categoryNotFound'), 401);
        }
    }

    public function products($id)
    {
        try {
            $category = Category::findByIdOrSlugOrFail($id);
            return new ProductCollection($category->products()->with('images')->where('status', '>', '0')->orderByDesc('priority')->get());
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.categoryNotFound'), 401);
        }
    }

}
