<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\DeliveryCollection;
use App\Http\Resources\DeliveryResource;
use App\Models\Delivery;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return DeliveryCollection
     */
    public function index()
    {
        return new DeliveryCollection(Delivery::all());
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return DeliveryResource
     */
    public function show($id)
    {
        try {
            $delivery = Delivery::findOrFail($id);
            return new DeliveryResource($delivery);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

}
