<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CreateCommentRequest;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\CommentResource;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Comment;
use App\Models\Product;
use App\Models\Rate;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->only(['followOrUnfollow', 'submitComment']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return ProductCollection
     */
    public function index(Request $request)
    {
        try {
            $products = Product::with(['images'])->where('status', '>', '0');
            $products = $request->input('startPrice') != null ? $products->where('price', '>=', $request->input('startPrice')) : $products;
            $products = $request->input('endPrice') != null ? $products->where('price', '<=', $request->input('endPrice')) : $products;
            $products = $request->input('subcategoryId') != null ? $products->where('subcategory_id', $request->input('subcategoryId')) : $products;
            $products = $request->input('title') != null ? $products->where('title', 'like', "%{$request->input('title')}%") : $products;
            $products = $request->input('orderBy') != null ? $products->orderBy($request->input('orderBy')) : $products->orderBy('priority');
            $products = $request->input('orderByDesc') != null ? $products->orderByDesc($request->input('orderByDesc')) : $products->orderByDesc('priority');
            $products = $request->input('limit') != null ? $products->limit($request->input('limit')) : $products;
            return new ProductCollection($products->get());
        } catch (QueryException $exception) {
            return falseJsonResponse("Bad query for search");
        }
    }


    /**
     * Display the specified resource.
     *
     * @param int | string $id
     * @return ProductResource
     */
    public function show($id)
    {
        try {
            $product = Product::with(['subcategory.category'])
                ->where('status', '>', '0')
                ->findByIdOrSlugOrFail($id)
                ->load([ 'images', 'category']);
            return new ProductResource($product);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.bannerNotFound'), 401);
        }
    }

    /**
     * Follow or un follow the specified resource .
     *
     * @param int $id
     * @return ProductResource
     */
    public function followOrUnfollow($id)
    {
        try {
            $product = Product::findByIdOrSlugOrFail($id);
            return $product->followers()->toggle(auth('api')->id()) ? new ProductResource($product) : falseJsonResponse(__('message.api.somethingWasWrong'), 401);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.bannerNotFound'), 401);
        }
    }


    public function search(Request $request)
    {
        $products = Product::search($request->input('s'))->where('status', '>', '0')->load('comments')->get();
        return new ProductCollection($products);

    }


    public function suggestions($id)
    {
        try {
            $product = Product::with('suggestions.images')
                ->where('status', '>', '0')
                ->findByIdOrSlugOrFail($id);
            return new ProductCollection($product->suggestions);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    public function relatedProduct($id)
    {
        $product = Product::with('subcategory.products.images')
            ->where('status', '>', '0')
            ->findByIdOrSlugOrFail($id);
        return new ProductCollection($product->subcategory->products);
    }

    public function comments($id)
    {
        $comments = Product::findByIdOrSlugOrFail($id)
            ->comments()
            ->where('approved','1')
            ->get();
        return new CommentCollection($comments);
    }


    public function submitComment($id, CreateCommentRequest $request)
    {
        try {
            $product = Product::findByIdOrSlugOrFail($id);
            $comment = new Comment(['text' => $request->input('text')]);
            $comment->user_id = auth('api')->id();
            $comment->is_buyer = !!$product->buyers()->where('id', $comment->user_id)->count();
            return $product->comments()->save($comment) ? new CommentResource($comment) : falseJsonResponse(__('message.api.somethingWasWrong'), 401);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.somethingWasWrong'), 401);
        }
    }

    public function submitRate($id,Request $request)
    {
        try {
            $product = Product::findByIdOrSlugOrFail($id);
            $rate = new Rate(['rate' => $request->input('rate')]);
            $rate->user_id = auth('api')->id();
            return $product->rate()->save($rate) ? trueJsonResponse("Your rate submitted successfully") : falseJsonResponse(__('message.api.somethingWasWrong'), 401);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.somethingWasWrong'), 401);
        }
    }

    public function suggested()
    {
        return new ProductCollection(Product::with(['images'])->where('suggested', 1)->orderBy('priority')->orderByDesc('created_at')->get());
    }
    public function popular()
    {
        return new ProductCollection(Product::with('images')->get()->sortByDesc('avgRates')->values()->take(10));
    }

}
