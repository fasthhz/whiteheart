<?php

namespace App\Http\Controllers\Api;

use App\Models\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class AppInfoController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return trueJsonResponse('', 202, App::info());
    }

    public function aboutUs()
    {
        return trueJsonResponse('', 202, App::info()['aboutUs']);
    }

    public function branches()
    {
        return trueJsonResponse('', 202, App::info()['agencies']);
    }

    public function mainCarousel()
    {
        return trueJsonResponse('', 202, App::info()['mainCarousel']);
    }

    public function secondCarousel()
    {
        return trueJsonResponse('', 202, App::info()['secondCarousel']);
    }

    public function socialNetworks()
    {
        return trueJsonResponse('', 202, App::info()['socialNetworks']);
    }


    public function baseInfo()
    {
        return trueJsonResponse('', 202, [
            'webSite' => App::info()['websiteUrl'],
            'email' => App::info()['email'],
            'socialNetworks' => App::info()['socialNetworks'],
            'developedBy' => App::info()['developedBy'],
            'links' => App::info()['links'],
            'homePics' => App::info()['homePics']]);

    }
}
