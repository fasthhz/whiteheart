<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\SelectCityRequest;
use App\Http\Requests\SelectLocationRequest;
use App\Models\Branch;
use http\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class MapController extends Controller
{
    public function index()
    {

    }
    function distance($lat1, $lon1, $lat2, $lon2,$unit="K") {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }
    public function selectLocation(SelectLocationRequest $request)
    {
        $branches = Branch::query()->where('state',$request->city)->get();
        $lats = $branches->pluck('lat')->toArray();
        $lngs = $branches->pluck('lng')->toArray();
        // latitude and longitude of Two Points
        $latitudeFrom = $request->lat;
        $longitudeFrom = $request->lng;
        $distancesArr = [];
        $map= [];
        $branch_id = 0;
        foreach ($branches as $key => $branch){
            $map[$key] = $this->distance($latitudeFrom, $longitudeFrom , $branch->lat,$branch->lng);
            $map[$key] = $branch->id;
            $branch_id = $branch->id;
            array_push($distancesArr,$map);
        }
        $distance = 2000;
//        dd($map);

        return response()->json([
            'success'=>true,
            'provider'=> [
                'provider_id'=>1,
                'provider_title'=>'شعبه اصفهان - مصلی',
                'provider_phone'=>'0211111111',
            ],
            'distance'=>$distance,
            'branch_id'=>$branch_id,
            'message'=>'تست متن پیام'
        ]);
    }

    public function selectCity(SelectCityRequest $request)
    {
        $lat=0;
        $lng=0;
        if($request->city){
            switch ($request->city){
                case 'tehran':
                    $lat='35.69940939470653';$lng='51.39489322900773';
                    break;
                case 'esfehan':
                    $lat='32.660682387769334';$lng='51.666523218154914';
                    break;
                default:
                    return 'not_defined';
            }
            return response()->json([
                'success'=>true,
                'lat'=>$lat,
                'lng'=>$lng
            ]);
        }
        return 'not_found';
    }
}
