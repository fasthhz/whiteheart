<?php

namespace App\Http\Controllers\Api;

use App\Events\Banner\InvoiceCreated;
use App\Exceptions\DiscountNotValid;
use App\Exceptions\InvalidItemException;
use App\Exceptions\NotEnoughQuantityException;
use App\Http\Requests\CreateInvoiceRequest;
use App\Http\Resources\InvoiceCollection;
use App\Http\Resources\InvoiceResource;
use App\Models\Delivery;
use App\Models\Discount;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\User;
use App\Models\UserAddress;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    /**
     * @var User
     */
    protected $user;

    public function __construct()
    {
        $this->middleware('auth:api')->only(['store', 'show', 'index']);
        $this->user = auth('api')->user();
    }

    /**
     * Display all resource.
     *
     * @param Request $request
     * @return InvoiceCollection
     */
    public function index(Request $request)
    {
        $invoices = Invoice::where('user_id', auth('api')->id())->with('products')->orderByDesc('created_at');
        $perPage = $request->perPage ?: 10;
        return new InvoiceCollection($invoices->paginate($perPage));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateInvoiceRequest $request
     * @return InvoiceResource
     * @throws \Throwable
     */
    public function store(CreateInvoiceRequest $request)
    {

        try {
            $products = $request->products();
            DB::beginTransaction();
            $address = UserAddress::findOrFail($request->input('address'));
//            $discount = Discount::findByCodeOrFail($request->input('discount'));
            $invoice = new Invoice([
                'address' => $address->forInvoice(),
                'description' => $request->input('description'),
            ]);
            $invoice->status = 0;
            $this->user->invoices()->save($invoice);
            foreach ($products as $item) {
                $product = Product::findOrFail($item[0]);
                $product->buy($item[1]);
                $invoice->addItemWithOutSave($product, $item[1]);
                $invoice->products()->attach($product->id, ['count' => $item[1]]);
            }
//            $invoice->setDelivery($delivery);
//           !$discount ?: $invoice->useDiscount($discount);
            if (!$invoice->update()) {
                DB::rollBack();
                return falseJsonResponse(__('message.api.invoiceCouldNotCreate'));
            }
            DB::commit();
            event(new InvoiceCreated($invoice));
            return new InvoiceResource($invoice);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.modelNotFound'));
        } catch (InvalidItemException $exception) {
            return falseJsonResponse(__('message.api.invalidItem'));
        } catch (DiscountNotValid $exception) {
            return falseJsonResponse(__('message.api.discountNotValid'));
        } catch (NotEnoughQuantityException $exception) {
            return falseJsonResponse(__('message.api.notEnoughQuantity', ['product' => $exception->product->title]));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return InvoiceResource
     */
    public function show($id)
    {
        try {
            $invoice = Invoice::with('products')->where('user_id', auth('api')->id())->findOrFail($id);
            return new InvoiceResource($invoice);
        } catch (ModelNotFoundException $exception) {
            return falseJsonResponse(__('message.api.invoiceNotFound'), 401, null, 404);
        }
    }


    public function payWithCreditCard($id)
    {

        $invoice = Invoice::findOrFail($id);
        if ($gateway = $invoice->payWithCreditCard())
            return $gateway->redirect();
        return falseJsonResponse(__('message.api.somethingWasWrong'));

    }

}
