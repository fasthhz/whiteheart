<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use App\Notifications\SendVerifyCode;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AuthenticatedSessionController extends Controller
{

    public function __construct()
    {
//        $this->middleware('OTPTime',['sendOtpAgain']);
    }

    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        if(Session::get('login-step')==null){
            Session::put('newRegister',null);

            $user = User::query()->where('mobile',$request['mobile'])->first();
            if(!$user){
                Session::put('newRegister',true);
                $user = User::query()->create([
                  'name' => 'unknown',
                  'family' => 'unknown',
                  'mobile' => $request->mobile
                ]);
            }
            $user->notify(new SendVerifyCode($user));
            Session::put('user_login_id',$user->id);
            Session::put('login-step',2);
            return redirect()->back();
        }
        if(Session::get('login-step')==2){
//            $validatedData = Validator::make($request->toArray(),[
//                'code' => 'required'
//            ]);
            $user = User::query()->find(Session::get('user_login_id'));

            $generatedCode = Cache::get("MobileVerifyCode|{$user->mobile}");

            if($user && (isset($generatedCode) && $request->code == $generatedCode)){
                if($request->has('name')&&$request->has('family'))
                {
                    $user->update([
                        'name'=>$request->name,
                        'family'=>$request->family,
                    ]);
                }
                Auth::loginUsingId($user->id,true);
            }

            return redirect()->back()->withErrors(['message'=>'کد وارد شده اشتباه می باشد.']);


        }



        // $request->session()->regenerate();
        if(!empty($request->redirect_to))
        {
            return redirect($request->redirect_to);
        }
        else
        {
         return redirect()->route('select.branch');
        }
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Session::forget('user_login_id');
        Session::forget('login-step');
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');

    }

    public function sendOtpAgain(Request $request)
    {
        $user = User::query()->findOrFail($request->session()->get('user_login_id'));
        $user->notify(new SendVerifyCode($user));

    }
}
