<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;

class ImageController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $image = Image::findOrFail($id);
            return $image->download();
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function download($id)
    {
        try {
            $image = Image::findOrFail($id);
            return $image->download();
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }


}
