<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.admin.index_dashboard', ['users' => Admin::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.admin.create_dashboard');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $info = $request->except(['_token', 'password']);
            $admin = new Admin($info);
            $admin->password = bcrypt($request->input('password'));
            $admin->save() ? flash()->success('ادمین با موفقیت ثبت شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } catch (Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } finally {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $user = Admin::findOrFail($id);
            return view('admin.admin.show_dashboard', ['user' => $user]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $admin = Admin::with('roles')->findOrFail($id);
            $roles = Role::whereNotIn('id', $admin->roles->pluck('id')->toArray())->get();
            return view('admin.admin.edit_dashboard', ['user' => $admin, 'roles' => $roles]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $status = false;
        try {
            $admin = Admin::with('roles')->findOrFail($id);
            if ($request->input('addRole')) {
                $role = Role::findOrFail($request->input('item'));
                $status = $admin->assignRole($role);
            } elseif ($request->input('deleteRole')) {
                $role = Role::findOrFail($request->input('item'));
                $status = $admin->removeRole($role);
            } else {
                $admin->password = $request->input('password') ? bcrypt($request->input('password')) : $admin->password;
                $status = $admin->update($request->except(['_token', 'password']));
            }
            $status ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return redirect()->route('admin.dashboard.admins.edit', [ $admin]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        } catch (Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        try {
            $admin = Admin::findOrFail($id);
            $admin->delete() ? flash()->success('فاکتور با موقیت حذف شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return redirect()->route('admin.dashboard.admins.index');
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }


}
