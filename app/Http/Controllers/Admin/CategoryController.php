<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Image;
use App\Models\Option;
use App\Models\Subcategory;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::orderByDesc('priority')->get();
        return view('admin.category.index_dashboard', ['categories' => $categories]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $category = new Category($request->all());
        $category->save() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        return redirect()->route('admin.dashboard.categories.edit',$category->id);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $category = Category::with(['subcategories', 'image','products'])->findOrFail($id);
            return view('admin.category.show_dashboard', ['category' => $category]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $category = Category::with(['image'])->findOrFail($id);
            $subcategories = Subcategory::whereNotIn('id',$category->subcategories->pluck('id')->toArray())->get();
            return view('admin.category.edit_dashboard', ['category' => $category, 'subcategories' => $subcategories]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function update(Request $request, $id)
    {
        try {
            $category = Category::findOrFail($id);
            if ($request->input('addImage')) {
                $category->attacheUploadedImage($request->file('image'));
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            } elseif ($request->input('deleteImage')) {
                $image = Image::findOrFail($request->input('image'));
                $image->delete() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            } elseif ($request->input('addSubcategories')) {
                $category->options()->attach($request->input('option'));
            } elseif ($request->input('deleteSubcategories')) {
                $category->options()->detach($request->input('option'));

            } else {
                $category->update($request->all());
            }
            flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            return redirect()->route('admin.dashboard.categories.edit', [$id]);

        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     * @throws Exception
     */
    public function destroy($id)
    {
        try {
            $category = Category::findOrFail($id);
            $category->delete() ? flash()->success('دسته با موفقیت حذف شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } catch (ModelNotFoundException $exception) {
            abort(404);
        } catch (Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } finally {
            return redirect()->route('admin.dashboard.categories.index');
        }
    }
}
