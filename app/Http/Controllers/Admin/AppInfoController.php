<?php

namespace App\Http\Controllers\Admin;

use App\Models\App;
use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class AppInfoController extends Controller
{


    /**
     * Display the specified resource.
     *
     * @return Response
     */
    public function show()
    {
        return view('admin.appInfo.show_dashboard', ['info' => App::info()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit()
    {
        return view('admin.appInfo.edit_dashboard', ['info' => App::info()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $status = false;
        if ($request->input('addMainCarousel')) {
            $status = App::addMainCarousel($request->all());
        } elseif ($request->input('deleteMainCarousel')) {
            $status = App::deleteMainCarousel($request->input('carousel'));
        } elseif ($request->input('addSecondCarousel')) {
            $status = App::addSecondCarousel($request->all());
        } elseif ($request->input('deleteSecondCarousel')) {
            $status = App::deleteSecondCarousel($request->input('carousel'));
        } elseif ($request->input('addAgency')) {
            $addressDetails = explode("\n", $request->input('address'));
            $address = ['text_fa' => $addressDetails[0], 'text_en' => $addressDetails[1], 'latitude' => $addressDetails[2], 'longitude' => $addressDetails[3]];
            $numbers = explode("\n", $request->input('numbers'));
            $info = $request->all();
            $info['address'] = $address;
            $info['numbers'] = $numbers;
            $status = App::addAgency($info);
        } elseif ($request->input('deleteAgency')) {
            $status = App::deleteAgency($request->input('agency'));
        } elseif ($request->input('addSocialNetwork')) {
            $status = App::addSocialNetwork($request->all());
        } elseif ($request->input('deleteSocialNetwork')) {
            $status = App::deleteSocialNetwork($request->input('socialNetwork'));
        } elseif ($request->input('aboutUsHeader')) {
            $status = App::updateAboutUsHeader(['fa' => $request->input('fa'), 'en' => $request->input('en'), 'ar' => $request->input('ar'), 'image' => $request->file('image')]);
        } elseif ($request->input('aboutUsTitle')) {
            $status = App::updateAboutUsTitle(['fa' => $request->input('fa'), 'en' => $request->input('en'), 'ar' => $request->input('ar')]);
        } elseif ($request->input('aboutUsBody')) {
            $status = App::updateAboutUsBody(['fa' => $request->input('fa'), 'en' => $request->input('en'), 'ar' => $request->input('ar'), 'image' => $request->file('image')]);
        } elseif ($request->input('addHomePic')) {
            $status = App::addHomePic( $request->file('image'),  $request->input('link'));
        } elseif ($request->input('deleteHomePic')) {
            $status = App::deleteHomePic($request->input('image'));
        } elseif ($request->input('addLink')) {
            $status = App::addLink($request->all());
        } elseif ($request->input('deleteLink')) {
            $status = App::deleteLink($request->input('link'));
        } else {
            $info = [
                'websiteUrl' => $request->input('websiteUrl'),
                'email' => $request->input('email'),
                'developedBy' => ['title' => $request->input('developerTitle'), 'link' => $request->input('developerLink'), 'image' => $request->input('developerImage')]];
            $status = App::update($info);
        }
        $status ? flash()->success('اطلاعات با موفقیت به روز شد.') : flash()->error('خطایی در بروزرسانی اطلاعت رخ داده است.');

        return redirect()->route('admin.dashboard.appInfo.edit');
    }


}
