<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.user.index_dashboard', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.user.create_dashboard');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $info = $request->except('_token');
        try {
            $user = new User($info);
            $user->save() ? flash()->success('کاربر با موفقیت ثبت شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } catch (Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } finally {
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $user = User::with(['invoices.delivery'])->findOrFail($id);
            return view('admin.user.show_dashboard', ['user' => $user]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $user = User::findOrFail($id);
            return view('admin.user.edit_dashboard', ['user' => $user]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->update($request->except('_token')) ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return view('admin.user.edit_dashboard', ['user' => $user]);
        } catch (ModelNotFoundException $exception) {
            return abort(404);
        } catch (Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            return back();
        }
    }


    /**
     * Show the form for searching the specified resource.
     * @param Request $request
     * @return Factory|View
     */
    public function search(Request $request)
    {
        $users = User::orderByDesc('created_at');
        !$request->input('id') ?: $users->where('id', $request->input('id'));
        !$request->input('name') ?: $users->where('name', 'like', "%{$request->input('name')}%");
        !$request->input('family') ?: $users->where('family', 'like', "%{$request->input('family')}%");
        !$request->input('mobile') ?: $users->where('mobile', $request->input('mobile'));
        return view('admin.user.search_dashboard', ['users' => $users->get()]);
    }


}
