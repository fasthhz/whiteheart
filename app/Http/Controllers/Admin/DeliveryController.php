<?php

namespace App\Http\Controllers\Admin;

use App\Models\Delivery;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $deliveries = Delivery::all();
        return view('admin.delivery.index_dashboard', ['deliveries' => $deliveries]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function store(Request $request)
    {
        $delivery = new Delivery($request->all());
        $delivery->save() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        return redirect()->route('admin.dashboard.deliveries.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function show($id)
    {
        try {
            $delivery = Delivery::with('invoices')->findOrFail($id);
            return view('admin.delivery.show_dashboard', ['delivery' => $delivery]);

        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        try {
            $delivery = Delivery::findOrFail($id);
            return view('admin.delivery.edit_dashboard', ['delivery' => $delivery]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Factory|View
     */
    public function update(Request $request, $id)
    {
        try {
            $delivery = Delivery::findOrFail($id);
            if ($delivery->update($request->all()))
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            return redirect()->route('admin.dashboard.deliveries.edit', ['id' => $delivery->id]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Factory|View
     */
    public function destroy($id)
    {
        try {
            $delivery = Delivery::findOrFail($id);
            $delivery->delete() ? flash()->success('کد تخفیف با موفقیت حذف شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } catch (ModelNotFoundException $exception) {
            abort(404);
        } catch (\Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } finally {
            return redirect()->route('admin.dashboard.deliveries.index');
        }
    }
}
