<?php

namespace App\Http\Controllers\Admin;

use App\Festival;
use App\Models\Image;
use App\Models\Pistachio;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class FestivalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $festivals = Festival::all();
        return view('admin.festival.index_dashboard', ['festivals' => $festivals]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $festival = new Festival($request->all());
        $festival->save() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        return redirect()->route('admin.dashboard.festivals.edit', ['id' => $festival->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $festival = Festival::with(['pistachios', 'image'])->findOrFail($id);
            return view('admin.festival.show_dashboard', ['festival' => $festival]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        try {
            $festival = Festival::with(['image'])->findOrFail($id);
            $pistachios = Pistachio::whereNotIn('id', $festival->pistachios()->pluck('id')->toArray())->get();
            return view('admin.festival.edit_dashboard', ['festival' => $festival, 'pistachios' => $pistachios]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {

        try {
            $festival = Festival::findOrFail($id);
            if ($request->input('addImage')) {
                $festival->attacheUploadedImage($request->file('image'));
                flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            } elseif ($request->input('deleteImage')) {
                $image = Image::findOrFail($request->input('image'));
                $image->delete() ? flash()->success('اطلاعات با موفقیت به روزرسانی شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
            }elseif ($request->input('addItem')) {
                $festival->pistachios()->attach($request->input('item'));
            }elseif ($request->input('deleteItem')) {
                $festival->pistachios()->detach($request->input('item'));
            } else {
                $festival->update($request->all());
            }
            flash()->success('اطلاعات با موفقیت به روزرسانی شد');
            return redirect()->route('admin.dashboard.festivals.edit', ['id' => $id]);
        } catch (ModelNotFoundException $exception) {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $festival = Festival::findOrFail($id);
            $festival->delete() ? flash()->success('فستیوال با موفقیت حذف شد') : flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } catch (ModelNotFoundException $exception) {
            abort(404);
        } catch (\Exception $exception) {
            flash()->error('اطلاعات وارد شده صحیح نمی باشد');
        } finally {
            return redirect()->route('admin.dashboard.festivals.index');
        }
    }
}
