<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/v1', 'as' => 'v1.'], function () {
    Route::post('/select_location','Api\MapController@selectLocation')->name('select_location');
    Route::post('/select_city','Api\MapController@selectCity')->name('select_city');
    Route::group(['prefix' => '/user', 'as' => 'user.'], function () {
        Route::get('/comments', 'Api\UserController@comments');
        Route::delete('/comments/{id}', 'Api\UserController@deleteComment');
        Route::post('/', 'Api\UserController@store');
        Route::get('/', 'Api\UserController@show');
        Route::put('/', 'Api\UserController@update');
        Route::post('/login', 'Api\UserController@login');
        Route::get('/verifyMobile', 'Api\UserController@getVerifyCode');
        Route::post('/verifyMobile', 'Api\UserController@verifyMobile');
        Route::get('/addresses/', 'Api\UserAddressController@index');
        Route::post('/addresses', 'Api\UserAddressController@store');
        Route::get('/addresses/{id}', 'Api\UserAddressController@show');
        Route::put('/addresses/{id}', 'Api\UserAddressController@update');
        Route::delete('/addresses/{id}', 'Api\UserAddressController@destroy');

    });
    Route::get('/categories/{id}/products', 'Api\CategoryController@products');
    Route::apiResource('/categories', 'Api\CategoryController')->only(['show', 'index']);
    Route::apiResource('/subcategories', 'Api\SubCategoryController')->only(['show', 'index']);
    Route::apiResource('/festivals', 'Api\FestivalController')->only(['show', 'index']);
    Route::get('/products/suggested', 'Api\ProductController@suggested');
    Route::get('/products/popular', 'Api\ProductController@popular');
    Route::apiResource('/products', 'Api\ProductController');

    Route::get('/products/{id}/suggestions', 'Api\ProductController@suggestions');
    Route::get('/products/{id}/related', 'Api\ProductController@relatedProduct');
    Route::get('/products/{id}/comments', 'Api\ProductController@comments');
    Route::post('/products/{id}/comments', 'Api\ProductController@submitComment');
    Route::get('/products/{id}/followOrUnfollow', 'Api\ProductController@followOrUnfollow');
    Route::apiResource('/deliveries', 'Api\DeliveryController')->only(['show', 'index']);

    Route::get('/invoices', 'Api\InvoiceController@index');
    Route::post('/invoices', 'Api\InvoiceController@store');
    Route::get('/invoices/{id}', 'Api\InvoiceController@show');
    Route::get('/invoices/{id}/pay', 'Api\InvoiceController@payWithCreditCard');
    Route::get('/discount/check', 'Api\DiscountController@check');
    Route::apiResource('/contactUs', 'Api\ContactUsController')->only(['store']);

    Route::get('/branches/{id}/products', 'Api\BranchController@products');
    Route::get('/branches', 'Api\BranchController@index')->name('branches');
    Route::get('/branches/{id}', 'Api\BranchController@show');


    Route::group(['prefix' => '/appInfo', 'as' => 'app.'], function () {
        Route::get('/', 'Api\AppInfoController@show');
        Route::get('/branches', 'Api\AppInfoController@branches');
        Route::get('/aboutUs', 'Api\AppInfoController@aboutUs');
        Route::get('/socialNetworks', 'Api\AppInfoController@socialNetworks');
        Route::get('/mainCarousel', 'Api\AppInfoController@mainCarousel');
        Route::get('/secondCarousel', 'Api\AppInfoController@secondCarousel');
        Route::get('/developedBy', 'Api\AppInfoController@developedBy');
        Route::get('/homePics', 'Api\AppInfoController@homePics');
        Route::get('/baseInfo', 'Api\AppInfoController@baseInfo');

    });

    Route::get('/questions', 'Api\QuestionController@index');



});
